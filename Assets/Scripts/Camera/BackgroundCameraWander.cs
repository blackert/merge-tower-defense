﻿using System.Collections.Generic;
using UnityEngine;

using MEC;


public class BackgroundCameraWander : MonoBehaviour
{
    private Transform _cameraTransform;
    private Camera _camera;

    public int cameraGameplayXRotation;

    public GameObject initialCameraFocusPoint;

    public float movementTime = 2f;
    private Vector3 newPosition;
    private Vector3 dir = Vector3.zero;

    public float xMin, xMax, zMin, zMax;
    private float cameraAdjustSpeed = 1f;
    private bool disabled;
    // Start is called before the first frame update
    void Start()
    {
        _cameraTransform = CameraController.cameraControllerTransform;
        _camera = CameraController._camera;
        dir = new Vector3(1, 0, 1);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (disabled) return;
        newPosition = _cameraTransform.position + dir*movementTime;
        if (newPosition.x > xMax || newPosition.x < xMin) {
            dir = new Vector3(-dir.x, 0, dir.z);
        } if (newPosition.z > zMax || newPosition.z < zMin) {
            dir = new Vector3(dir.x, 0, -dir.z);
        }
        _cameraTransform.position += dir * Time.deltaTime * movementTime;
    }

    public void OnEnable() {
        Start();
        GameObject.Find("CameraParent").GetComponent<CameraController>().HandleUserInput(false);
        disabled = false;
        Timing.RunCoroutine(AdjustCameraHeight(20f, (CameraController.cameraController.xRotationMin + CameraController.cameraController.xRotationMax)/2f, (CameraController.cameraController.minZoom + CameraController.cameraController.maxZoom)/2f));
    }

    public void OnDisable() {
        // Disable LateUpdate function.
        disabled = true;
        CoroutineHandle cameraAdjusting = Timing.RunCoroutine(SlowlyAdjustCameraAndDestroy(CameraController.cameraController.xRotationMin));
        // GameObject.Find("CameraParent").GetComponent<CameraController>().HandleUserInput(true);
        // gameObject.SetActive(false);
    }

    IEnumerator<float> SlowlyAdjustCameraAndDestroy(float toDegree) {
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(AdjustCameraHeight(10f, toDegree, CameraController.cameraController.maxZoom)));
        yield return 0;
        GameObject.Find("CameraParent").GetComponent<CameraController>().HandleUserInput(true);
    }

    IEnumerator<float> AdjustCameraHeight(float speed, float angle, float height) {
        // GameObject.Find("Camera").GetComponent<RTSCameraController>().enabled = false;
        Vector3 target = new Vector3(
            initialCameraFocusPoint.transform.position.x, 
            _cameraTransform.position.y,
            initialCameraFocusPoint.transform.position.z);
        float adjustingTimeout = 1f; // In seconds.
        bool distOk = false;
        bool angleOk = false;
        bool heightOk = false;
        while ((!angleOk || !distOk || !heightOk) && adjustingTimeout > 0) {
            adjustingTimeout -= Time.deltaTime;
            if (Mathf.Abs(Vector3.Distance(_cameraTransform.position, target)) < 0.1f) distOk = true;
            if (Mathf.Abs(_cameraTransform.rotation.eulerAngles.x - angle) < 0.1f) angleOk = true;
            if (Mathf.Abs(_camera.orthographicSize - height) < 0.1f) heightOk = true;
            if (!distOk) {
                _cameraTransform.position = Vector3.Lerp(_cameraTransform.position, target, speed*Time.deltaTime);
            }
            if (!angleOk) {
                // float degreeToMove = (90-angle)*speed*Time.deltaTime;
                // _cameraTransform.rotation = _cameraTransform.rotation * Quaternion.AngleAxis(degreeToMove, Vector3.left);
                // Vector3 rotationValue = new Vector3((angle-90)*speed*Time.deltaTime, 0, 0);
                if (angle > _cameraTransform.eulerAngles.x) {
                    _cameraTransform.Rotate(Vector3.right, speed*Time.deltaTime);
                } else {
                    _cameraTransform.Rotate(Vector3.left, speed*Time.deltaTime);
                }
                // _cameraTransform.eulerAngles -= rotationValue;
            }
            if (!heightOk) {
                if (height > _camera.orthographicSize) {
                    _camera.orthographicSize += speed*Time.deltaTime;
                } else {
                    _camera.orthographicSize -= speed*Time.deltaTime;
                }
            }
            yield return Timing.WaitForOneFrame;
        }
    }
}
