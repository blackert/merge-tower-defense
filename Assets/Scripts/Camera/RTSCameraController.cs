﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTSCameraController : CameraController
{
    private Vector3 newPosition;
    private Vector3 newRotation;
    private Vector3 dragStartPos, dragCurrentPos;
    private Vector3 rotateStartPos, rotateCurrentPos;
    private bool posChanged, rotationChanged, zoomChanged;
    
    protected override void Start() {
        base.Start();

        newPosition = transform.position;
        // newRotation = transform.rotation;
        // newZoom = _camera.transform.localPosition;
        posChanged = false;
        rotationChanged = false;
        zoomChanged = false;
    }

    void OnEnable() {
        Start();
    }

    void Update() {
        HandleMouseInput();
        HandleKeyboardInput();
        UpdateCameraTransform();
    }

    override protected void UpdateCameraTransform() {
        if (posChanged) {
            newPosition.y = Mathf.Clamp(newPosition.y, minZoom, maxZoom);
            newPosition.x = Mathf.Clamp(newPosition.x, xBoundaryMin, xBoundaryMax);
            newPosition.z = Mathf.Clamp(newPosition.z, zBoundaryMin, zBoundaryMax);
            transform.position = newPosition;
            posChanged = false;
        }
        if (rotationChanged) {
            newRotation.x = Mathf.Clamp(newRotation.x, -5, 40);
            transform.rotation = Quaternion.Euler(newRotation.x, newRotation.y, 0);
            // _cameraTransform.rotation.SetEulerRotation = Mathf.Clamp(newRotation.x, 45, 80);
            // newRotation = transform.rotation.eulerAngles;
            // newRotation.x = _cameraTransform.rotation.eulerAngles.x;
            rotationChanged = false;
        }
        // if (zoomChanged) {
        //     transform.position = newZoom;
        //     zoomChanged = false;
        // }
    }

    override protected void HandleMouseInput() {
        if (Input.mouseScrollDelta.y != 0) {
            ZoomUpdate(Input.mouseScrollDelta.y * zoomAmount);
        }
        
        if (Input.GetMouseButtonDown(0)) {
            Plane plane = new Plane(Vector3.up, Vector3.zero);
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);

            float entry;
            if (plane.Raycast(ray, out entry)) {
                dragStartPos = ray.GetPoint(entry);
            }
        }
        if (Input.GetMouseButton(0)) {
            Plane plane = new Plane(Vector3.up, Vector3.zero);
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);

            float entry;
            if (plane.Raycast(ray, out entry)) {
                dragCurrentPos = ray.GetPoint(entry);
                PositionUpdate((dragStartPos - dragCurrentPos) / 10f);
            }
        }

        if (Input.GetMouseButtonDown(1)) {
            rotateStartPos = Input.mousePosition;
        }
        if (Input.GetMouseButton(1)) {
            rotateCurrentPos = Input.mousePosition;
            Vector3 dif = rotateStartPos - rotateCurrentPos;
            Vector3 correctedDif = new Vector3(-dif.y, -dif.x, -dif.z);
            rotateStartPos = rotateCurrentPos;
            // RotationUpdate((-dif.x/10f));
            RotationUpdate(correctedDif/10f);
        }
    }

    override protected void HandleKeyboardInput() {
        if (Input.GetAxis("Vertical") != 0) {
            PositionUpdate(transform.forward * movementSpeed * Input.GetAxis("Vertical"));
        }
        if (Input.GetAxis("Horizontal") != 0) {
            PositionUpdate(transform.right * movementSpeed * Input.GetAxis("Horizontal"));
        }

        if (Input.GetKey(KeyCode.Q)) {
            RotationUpdate(Vector3.up);
        }
        if (Input.GetKey(KeyCode.E)) {
            RotationUpdate(Vector3.down);
        }

        if (Input.GetKey(KeyCode.R)) {
            ZoomUpdate(zoomAmount);
        }
        if (Input.GetKey(KeyCode.F)) {
            ZoomUpdate(-zoomAmount);
        }
    }

    void ZoomUpdate(float zoomAmount) {
        //
        // LET IT BE FOR NOW. IT CAN BE DELETED IN FAVOR OF 'PositionUpdate' in the future.
        //

        // newZoom += zoomAmount;
        // newZoom.y = Mathf.Clamp(newZoom.y, minZoom, maxZoom);
        newPosition.y += zoomAmount; // Is this working??
        // zoomChanged = true;
        posChanged = true;
    }

    void PositionUpdate(Vector3 posChangeAmount) {
        newPosition += posChangeAmount;
        posChanged = true;
    }

    void RotationUpdate(Vector3 rotChange) {
        // newRotation *= Quaternion.Euler(Vector3.up * rotChangeFromUp);
        newRotation += rotChange;
        rotationChanged = true;
    }
}
