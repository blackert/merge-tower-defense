﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController cameraController;
    public static Transform cameraControllerTransform;
    public static Camera _camera;
    public static Transform _cameraTransform;

    protected bool handleUserInput;


    public float movementSpeed, movementTime;
    public float xBoundaryMin, xBoundaryMax, zBoundaryMin, zBoundaryMax;

    public float rotationAmount;
    public float xRotationMin, xRotationMax;

    public float zoomAmount;
    public float minZoom, maxZoom;

    protected virtual void Start() {
        _camera = Camera.main;
        _cameraTransform = _camera.transform;
        cameraController = this;
        cameraControllerTransform = this.transform;
    }

    protected virtual void HandleMouseInput() {

    }

    protected virtual void HandleKeyboardInput() {

    }

    protected virtual void UpdateCameraTransform() {

    }

    public void HandleUserInput(bool handle) {
        handleUserInput = handle;
    }
}
