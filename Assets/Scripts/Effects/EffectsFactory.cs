using System.Collections.Generic;
using UnityEngine;

using MEC;

public enum EffectType {
    Bullet = 0,
    Missile = 1
}

public class EffectsFactory : MonoBehaviour
{
    public static EffectsFactory effectsFactory;

    public Transform EffectParent;

    public List<GameObject> effectTypes;
    public float effectDuration = 5f;
    public int maxEffectInstancesInCache = 5000;
    public int burstCacheFilling = 5;

    public List<List<GameObject>> effectCache;

    public List<int> lastAccessedEffectIndexInCache;

    void Start() {
        effectsFactory = this;
        CreateCacheSpaceForEffects();
        CoroutineHandle effectCacheCreateCoroutine = Timing.RunCoroutine(_populateCache());
        // ClearCache();
    }

    public void ShowEffect(EffectType effectType, Transform where) {
        int effectTypeIdx = (int)effectType;
        GameObject fetchedEffect = effectCache[effectTypeIdx][lastAccessedEffectIndexInCache[effectTypeIdx]];
        lastAccessedEffectIndexInCache[effectTypeIdx] = (lastAccessedEffectIndexInCache[effectTypeIdx] + 1) % effectCache[effectTypeIdx].Count;
        fetchedEffect.transform.position = where.position;
        fetchedEffect.transform.rotation = where.rotation;
        Timing.RunCoroutine(_showEffectForSeconds(effectDuration, fetchedEffect));
    }

    private void CreateCacheSpaceForEffects() {
        int effectTypeCount = effectTypes.Count;
        effectCache = new List<List<GameObject>>();
        lastAccessedEffectIndexInCache = new List<int>();
        for (int i = 0; i < effectTypeCount; i++) {
            effectCache.Add(new List<GameObject>());
            lastAccessedEffectIndexInCache.Add(0);
        }
    }

    private void ClearCache() {
        for (int i = 0; i < effectCache.Count; i++) {
            ClearCacheEffectType(i);
        }
    }

    private void ClearCacheEffectType(int effectTypeIdx) {
        Debug.Log("Before clear on row " + effectTypeIdx + " size:" + effectCache[effectTypeIdx].Count);
        foreach (GameObject effectInstance in effectCache[effectTypeIdx]) {
            Destroy(effectInstance);
        }
        Debug.Log("After clear on row " + effectTypeIdx + " size:" + effectCache[effectTypeIdx].Count);
    }

    private GameObject spawnEffect(GameObject effect) {
        GameObject spawnedEffect = (GameObject) Instantiate(effect, EffectParent.transform);
        spawnedEffect.SetActive(false);
        return spawnedEffect;
    }

    private IEnumerator<float> _showEffectForSeconds(float howLong, GameObject effect) {
        effect.SetActive(true);
        effect.GetComponent<ParticleSystem>().Play();
        yield return Timing.WaitForSeconds(howLong);
        effect.SetActive(false);
    }

    private IEnumerator<float> _populateCache() {
        int effectsRemainingInBurst = burstCacheFilling;
        for(int i = 0; i < effectCache.Count; i++) {
            GameObject effect = effectTypes[i];
            List<GameObject> cache = effectCache[i];
            lastAccessedEffectIndexInCache[i] = 0;
            for(int j = 0; j < maxEffectInstancesInCache; j++) {
                cache.Add(spawnEffect(effect));
                if (effectsRemainingInBurst <= 0) {
                    effectsRemainingInBurst = burstCacheFilling;
                    yield return Timing.WaitForOneFrame;
                }
                effectsRemainingInBurst--;
            }
        }
    }
}