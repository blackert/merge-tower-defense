﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tower Blueprinty", menuName = "Tower Blueprinty", order = 51)][InitializeOnLoad]
public class TowerBlueprinty : ScriptableObject
{
    public GameObject towerPrefab;
    public TowerType towerType;
    public AmmoType ammoType;
    public EffectType ammoEffectType;
    public int power;
    public static Dictionary<int, int> purchasePrices = new Dictionary<int, int>() {
        {2, 50}
    };

    static TowerBlueprinty() {
        if (purchasePrices.Count < Mathf.Log(2048, 2)) {
            purchasePrices.Clear();
            purchasePrices.Add(2, 50);
            for(int i = 2; i <= 11; i++) {
               purchasePrices.Add((int)Mathf.Pow(2, i), (int)(purchasePrices[(int)Mathf.Pow(2, i-1)]*1.9f));
            }
        }
    }

    public int GetPurchasePrice() {
        return purchasePrices[power];
    }

    public int GetSellPrice() {
        return (int)(purchasePrices[power]*0.9f);
    }
}
