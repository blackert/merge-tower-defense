﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameMain;
using MEC;
using UnityEngine;
using UnityEngine.UI;

public class TowerController : MonoBehaviour
{
    public static TowerController towerController;

    public TowerBlueprinty[] lightHitterTowers;
    public TowerBlueprinty[] normalHitterTowers;
    public TowerBlueprinty[] heavyHitterTowers;
    public TowerBlueprinty[] magicHitterTowers;
    
    private GameObject purchasedTower;
    private TowerBlueprinty purchasedTowerBlueprint;
    public Vector3 purchasedTowerInitialPosition;
    public Button cancelPurchaseUIButton;
    private bool purchasedATower;

    public float speed = 15.0f;
    public float cancelSelectionTimeout = 2.0f;
    private float heightWhenTowerSelected = 2.5f;
    public bool materialSwitchWhenTowerSelected = false;

    private Ray ray;
    private RaycastHit hit;

    private GameObject touchedObject;
    private bool towerControllerIsBusy;

    public MergePhase mergePhase;

    private GameObject firstTower;
    private int firstTowerPower;
    private TowerType firstTowerType;
    private Transform firstTowerTransform;
    private Tower firstTowerScript;
    // private float firstTowerInitialYPos;
    private float firstTowerTimer;

    private GameObject secondTower;
    private int secondTowerPower;
    private TowerType secondTowerType;
    private Vector2 secondTowerVirtualPos;
    private Tower secondTowerScript;
    private Transform secondTowerTransform;
    // public float secondTowerInitialYPos;

    private float lastClickedTimer = -1;
 
     //Change me to change the touch phase used.
    //  TouchPhase touchPhase = TouchPhase.Ended;
 
    void Awake() {
        towerController = this;    
    }

    void Start() {
        mergePhase = MergePhase.StartingPhase;
        purchasedTowerInitialPosition = new Vector3(120, 0.5f, 120);
        // secondTowerSelected = false;
    }

    // private GameObject isAnyTowerTouched() {
    //     Debug.Log("Checking");
    //     ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
    //     Physics.Raycast(ray, out hit);
    //     if (hit.collider != null) {
    //         touchedObject = hit.transform.gameObject;
    //         Debug.Log("Touched " + touchedObject.transform.name);
    //         return touchedObject;
    //     }
    //     return null;
    // }

    // PC TEsting
    private bool IsAnyObjectClicked() {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit);
        if (hit.collider != null) {
            touchedObject = hit.transform.gameObject;
            return true;
        }
        return false;
    }

    public void EnableClickingOnTowers() {
        towerControllerIsBusy = false;
        lastClickedTimer = 0;
    }

    public void DisableClickingOnTowers() {
        towerControllerIsBusy = true;
    }

     void Update() {
        //  //We check if we have more than one touch happening.
        //  //We also check if the first touches phase is Ended (that the finger was lifted)
        //  if (Input.touchCount > 0 && Input.GetTouch(0).phase == touchPhase) {
        //      if (isAnyTowerTouched()) {
        //          ;
        //      }
        //  }

        // PC TEsting
        lastClickedTimer += Game.deltaTime;
        if (lastClickedTimer < 0.1f * Time.timeScale || towerControllerIsBusy || Game.IsPaused())
            return;
        if (mergePhase < MergePhase.SecondTowerSelected) {
            if (Input.GetMouseButtonUp(0)) {
                lastClickedTimer = 0;
                if (IsAnyObjectClicked()) {
                    switch(touchedObject.name) {
                        case string a when a.Contains("Tower"): clickedOnTower(); return;
                        case string b when b.Contains("Grass"): clickedOnGrass(); return;
                        case string c when c.Contains("Road") || c.Contains("Rock"): clickedOnUnplayableTile(); return;
                    }
                }
                firstTowerTimer = -5f * Time.timeScale; // Immediately cancel phase one.
            }
        }
        switch(mergePhase) {
            case MergePhase.StartingPhase: {
                if (purchasedATower) {
                    touchedObject = purchasedTower;
                    FirstTowerSelect();
                    firstTowerTimer = Time.time + 100f * Time.timeScale;
                    mergePhase++;
                }
                break;
            }
            case MergePhase.FirstTowerSelected: {
                if (Time.time - firstTowerTimer > cancelSelectionTimeout * Time.timeScale) {
                    mergePhase = MergePhase.StartingPhase;
                    FirstTowerDeselect();
                }
                break;
            }
            case MergePhase.SecondTowerSelected: {
                TowerInfoTooltipController.towerInfoTooltipController.DisableInfoTooltip();
                if (purchasedATower) usedPurchasedTower();
                Timing.RunCoroutine(_moveTower(firstTowerTransform, new Vector3(
                    secondTowerTransform.position.x,
                    secondTowerTransform.position.y,
                    secondTowerTransform.position.z)));
                mergePhase++;
                break;
            }
            case MergePhase.FirstTowerMovedToSecondTower: {
                DestroyTower(firstTower);
                DestroyTower(secondTower);
                mergePhase++;
                break;
            }
            case MergePhase.BuildNewTower: {
                Debug.Log("Tower 1 power: " + firstTowerPower + " Tower 2 power: " + secondTowerPower);
                Debug.Log("Building tower at index: " + (int) Mathf.Log(secondTowerPower, 2) + " and Type: " + secondTowerType);
                BuildTower(
                    GetTowerBlueprintyByType((int) Mathf.Log(secondTowerPower, 2), secondTowerType),
                    secondTowerVirtualPos);
                mergePhase = MergePhase.StartingPhase;
                break;
            }
        }
    }

    private void clickedOnUnplayableTile() {
        TowerInfoTooltipController.towerInfoTooltipController.DisableInfoTooltip();
    }

    private void clickedOnTower() {
        TowerInfoTooltipController.towerInfoTooltipController.DisableInfoTooltip();
        if (mergePhase < MergePhase.FirstTowerSelected) {
            Debug.Log("First Tower clicked");
            FirstTowerSelect();          
            mergePhase++;
        } else if (touchedObject != firstTower) {
            SecondTowerSelect();
            if (firstTowerPower != secondTowerPower || firstTowerType != secondTowerType) {
                // TODO: Print message to user.
                secondTowerDeselect();
                return;
            } else if (firstTowerPower == (int) TowerPower._2048) {
                secondTowerDeselect();
                return;
            }
            Debug.Log("Second Tower clicked");
            mergePhase++;
        }
    }

    private void clickedOnGrass() {
        TowerInfoTooltipController.towerInfoTooltipController.DisableInfoTooltip();
        if (mergePhase == MergePhase.FirstTowerSelected) {
            Vector3 localPos = touchedObject.transform.localPosition/GameBoard.gameBoard.tileScale;
            Vector2 localGridPos = new Vector2(localPos.x, localPos.z);
            if (!GameBoard.gameBoard.IsVirtualLocationPlayable(localGridPos)) {
                return;
            }
            Timing.RunCoroutine(_moveTowerOneAndDeselect(Timing.RunCoroutine(_moveTower(firstTowerTransform, new Vector3(touchedObject.transform.position.x, GETTowerDefaultYPos(firstTowerScript.getTowerBlueprinty()), touchedObject.transform.position.z)))));
            GameBoard.gameBoard.RelocateTowerToTile(firstTower, localGridPos);
            mergePhase--;
            if (purchasedATower) usedPurchasedTower();
        }
    }

    private void FirstTowerSelect() {
        firstTowerTimer = Time.time;
        firstTower = touchedObject;
        firstTowerTransform = firstTower.transform;
        // firstTowerInitialYPos = firstTowerTransform.position.y;
        firstTowerScript = firstTower.GetComponent<Tower>();
        // setTowerMaterial(firstTowerTransform, firstTowerScript.towerSelectedMaterial);
        // firstTower.GetComponent<Renderer>().material = firstTowerScript.towerSelectedMaterial;
        firstTowerPower = firstTowerScript.getTowerPower();
        firstTowerType = firstTowerScript.getTowerType();
        Timing.RunCoroutine(_moveTower(firstTowerTransform, new Vector3(
            firstTowerTransform.position.x,
            GETTowerDefaultYPos(firstTowerScript.getTowerBlueprinty()) + heightWhenTowerSelected,
            firstTowerTransform.position.z)));
        TowerInfoTooltipController.towerInfoTooltipController.ShowTowerInfo(firstTowerScript);
    }

    private void FirstTowerDeselect() {
        // setTowerMaterial(firstTowerTransform, firstTower.GetComponent<Tower>().towerNeutralMaterial);
        // firstTower.GetComponent<Renderer>().material = firstTower.GetComponent<Tower>().towerNeutralMaterial;
        Timing.RunCoroutine(_moveTower(firstTowerTransform, new Vector3(
            firstTowerTransform.position.x,
            GETTowerDefaultYPos(firstTowerScript.getTowerBlueprinty()),
            firstTowerTransform.position.z)));
    }

    private void SecondTowerSelect() {
        secondTower = touchedObject;
        secondTowerTransform = secondTower.transform;
        // secondTowerInitialYPos = secondTowerTransform.position.y;
        Tower secondTowerScript = secondTower.GetComponent<Tower>();
        secondTowerPower = secondTowerScript.getTowerPower();
        secondTowerType = secondTowerScript.getTowerType();
        secondTowerVirtualPos = secondTowerScript.getTowerVirtualGridLocation();
    }

    private void secondTowerDeselect() {
        // firstTowerDeselect();
    }

    private void SetTowerMaterial(Transform tower, Material newMaterial) {
        int numOfChildren = tower.childCount;
        for(int i = 0; i < numOfChildren; i++)
        {
            try {
                GameObject child = tower.GetChild(i).gameObject;
                if (!child.name.Contains("Text")) {
                    child.GetComponent<Renderer>().material = newMaterial;
                }
            } catch (MissingComponentException) {}
        }
    }

    public TowerBlueprinty GetTowerBlueprintyByType(int index, TowerType type) {
        switch(type) {
            case TowerType.LightHitter: {
                return lightHitterTowers[index];
            }
            case TowerType.NormalHitter: {
                return normalHitterTowers[index];
            }
            case TowerType.HeavyHitter: {
                return heavyHitterTowers[index];
            }
            default: Debug.LogError("Unknownt tower type! Error!"); break;
        }
        return null;
    }

    private void DestroyTower(GameObject tower) {
        GameBoard.gameBoard.RemoveTowerFromTile(tower);
        Destroy(tower);
    }    

    public Tower BuildTower(TowerBlueprinty tower, Vector2 gridPosition) {
        GameObject createdTower = GameBoard.gameBoard.CreateNewTower(tower, gridPosition);
        return createdTower.GetComponent<Tower>();
    }

    public static float GETTowerDefaultYPos(TowerBlueprinty towerBlueprinty) {
        if (towerBlueprinty.towerType == TowerType.LightHitter) {
            return 0.5f;
        } else if (towerBlueprinty.towerType == TowerType.NormalHitter) {
            return 1.6f;
        } else if (towerBlueprinty.towerType == TowerType.HeavyHitter) {
            return 1.1f;
        } else {
            return 0.5f;
        }
    }

    public void ShopTower(TowerBlueprinty tower) {
        // Debug.Log("Purchased tower is being created.");
        if (mergePhase == MergePhase.FirstTowerSelected) {
            // Debug.Log("Phase one was active. Disabling it.");
            mergePhase--;
            FirstTowerDeselect();
        }
        purchasedATower = true;
        purchasedTowerBlueprint = tower;
        purchasedTowerInitialPosition.y = GETTowerDefaultYPos(purchasedTowerBlueprint);
        // Debug.Log("Created the purchased tower at " + purchasedTowerInitialPosition);
        purchasedTower = GameBoard.gameBoard.CreateTower(tower, purchasedTowerInitialPosition);
    }

    private void usedPurchasedTower() {
        purchasedATower = false;
        // cancelPurchaseUIButton.OnSelect(null);
        cancelPurchaseUIButton.onClick.Invoke();
    }

    public void RefundTower() {
        if (purchasedATower) {
            Destroy(purchasedTower);
            purchasedATower = false;
            mergePhase = MergePhase.StartingPhase;
            Player.player.refundItem(purchasedTowerBlueprint.GetPurchasePrice());
        }
        else {
            Debug.Log("Purchased item is used already!");
        }
    }

    private IEnumerator<float> _moveTower(Transform towerTransform, Vector3 dist) {
        towerControllerIsBusy = true;
        while (Vector3.Distance(towerTransform.position, dist) >= 0.2f) {
            float step = speed * Game.deltaTime;
            towerTransform.position = Vector3.Lerp(towerTransform.position, dist, step);
            yield return Timing.WaitForOneFrame;
        }
        towerTransform.position = dist;
        towerControllerIsBusy = false;
    }

    private IEnumerator<float> _moveTowerOneAndDeselect(CoroutineHandle waitFor) {
        yield return Timing.WaitUntilDone(waitFor);
        FirstTowerDeselect();
    }

    public Tower UpgradeTower(Tower tower) {
        towerControllerIsBusy = true;
        var towerPower = tower.getTowerPower();
        var towerType = tower.getTowerType();
        var towerVirtualGridLocation = tower.getTowerVirtualGridLocation();
        DestroyTower(tower.getTowerGameObject());
        Tower upgradedTower = BuildTower(GetTowerBlueprintyByType((int) Mathf.Log(towerPower, 2), towerType), towerVirtualGridLocation);
        Player.player.boughtItem(tower.getTowerBlueprinty().GetPurchasePrice());
        mergePhase = MergePhase.StartingPhase;
        towerControllerIsBusy = false;
        return upgradedTower;
    }

    public void SellTower(Tower tower) {
        towerControllerIsBusy = true;
        var towerSellPrice = tower.getTowerBlueprinty().GetSellPrice();
        DestroyTower(tower.getTowerGameObject());
        Player.player.soldItem(towerSellPrice);
        mergePhase = MergePhase.StartingPhase;
        towerControllerIsBusy = false;
    }
}