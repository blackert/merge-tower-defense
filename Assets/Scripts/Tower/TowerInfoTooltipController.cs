using System.Collections;
using System.Collections.Generic;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TowerInfoTooltipController : MonoBehaviour
{
    public static TowerInfoTooltipController towerInfoTooltipController;

    public GameObject towerInfoCanvas;

    public TextMeshProUGUI towerNameText;
    public TextMeshProUGUI towerTypeText;
    public TextMeshProUGUI towerBulletTypeText;
    public TextMeshProUGUI towerAttackRangeText;
    public TextMeshProUGUI towerAttackDamageText;
    public TextMeshProUGUI towerUpgradeCostText;
    public TextMeshProUGUI towerSellCostText;

    private Tower lastTower;
    private bool isActive;

    void Awake() {
        towerInfoTooltipController = this;
        isActive = false;  
    }

    public void EnableInfoTooltip() {
        if (!isActive) {
            towerInfoCanvas.SetActive(true);
            isActive = true;
        }
    }

    public void DisableInfoTooltip() {
        if (isActive) {
            RemoveTowerRangeCircle(lastTower);
            towerInfoCanvas.SetActive(false);
            isActive = false;
        }
    }

    public void ShowTowerInfo(Tower tower) {
        lastTower = tower;
        towerNameText.SetText(tower.getTowerName());
        string towerTypeName;
        EnumLookupTables.towerTypeLookup.TryGetValue(tower.getTowerType(), out towerTypeName);
        towerTypeText.SetText(towerTypeName);
        string bulletTypeName;
        EnumLookupTables.ammoTypeLookup.TryGetValue(tower.getAmmoType(), out bulletTypeName);
        towerBulletTypeText.SetText(bulletTypeName);
        towerAttackRangeText.SetText(tower.getTowerRange().ToString("0"));
        towerAttackDamageText.SetText(tower.getTowerDamage().ToString("0"));
        if (tower.getTowerPower() == 2048) {
            towerUpgradeCostText.SetText("-");
            towerInfoCanvas.transform.Find("Info/TowerInfoPanel/TowerInfoThirdRow/Buttons/UpgradeButton").GetComponent<Button>().interactable = false;
        } else {
            towerUpgradeCostText.SetText(tower.getTowerBlueprinty().GetPurchasePrice().ToString("0"));
            towerInfoCanvas.transform.Find("Info/TowerInfoPanel/TowerInfoThirdRow/Buttons/UpgradeButton").GetComponent<Button>().interactable = true;
        }
        towerSellCostText.SetText((tower.getTowerBlueprinty().GetSellPrice()).ToString("0"));
        ShowTowerRangeCircle(tower);
        EnableInfoTooltip();
    }

    public void ShowTowerRangeCircle(Tower tower) {
        tower.DrawTowerRangeCircle();
    }

    public void RemoveTowerRangeCircle(Tower tower) {
        tower.DeleteTowerRangeCircle();
    }

    public void UpgradeTower() {
        Tower upgradedTower = TowerController.towerController.UpgradeTower(lastTower);
        ShowTowerInfo(upgradedTower);
    }

    public void SellTower() {
        DisableInfoTooltip();
        TowerController.towerController.SellTower(lastTower);
    }

}