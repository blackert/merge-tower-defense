﻿using System.Collections.Generic;
using GameMain;
using UnityEngine;

using MEC;

public class Turret : MonoBehaviour
{
    private Transform target = null;


    [Header("Attributes")]
    private float fireCountdown = 0f;
    [SerializeField] private float bulletDamage; // comes from tower level and any power-ups.
    [SerializeField] private float bulletSpeed; // Derived from weaponType and tower level.
    [SerializeField] private float bulletFireRate;
    [SerializeField] private float bulletRange;
    [SerializeField] private float bulletExplosionRadius;
    [SerializeField] private AmmoType ammoType;
    [SerializeField] private EffectType effectType;
    private Transform Enemies;
    [Header("Unity Setup")]
    public Transform firePoint;
    private GameObject towerRangeCircle;

    public void setTurretAttributes(float _bulletDamage, float _bulletSpeed, float _bulletFireRate, float _bulletRange, float _bulletExplosionRadius, AmmoType _ammoType, EffectType _effectType) {
        bulletDamage = _bulletDamage;
        bulletSpeed = _bulletSpeed;
        bulletFireRate = _bulletFireRate;
        bulletRange = _bulletRange;
        bulletExplosionRadius = _bulletExplosionRadius;
        ammoType = _ammoType;
        effectType = _effectType;
    }

    void Awake() {
        towerRangeCircle = GameObject.FindWithTag("TowerRangeCircle");
    }

    void Start() {
        Enemies = GameObject.FindGameObjectWithTag("Enemies").transform;
        Timing.RunCoroutine(UpdateTarget().CancelWith(gameObject));
    }

    IEnumerator<float> UpdateTarget() {
        Transform enemy;
        float shortestDistance = Mathf.Infinity;
        Transform closestEnemy = null;
        while (true) {
            shortestDistance = Mathf.Infinity;
            closestEnemy = null;
            for (int i = 0; i < Enemies.childCount; i ++) {
                enemy = Enemies.GetChild(i);
                if (enemy.GetComponent<BasicEnemy>().IsDead()) {
                    continue;
                }
                float distanceToEnemy = Vector3.Distance(transform.position, enemy.position);
                if (shortestDistance > distanceToEnemy) {
                    shortestDistance = distanceToEnemy;
                    closestEnemy = enemy;
                }
            }

            if (closestEnemy != null && shortestDistance <= bulletRange) {
                target = closestEnemy;
            } else {
                target = null;
            }
            yield return Timing.WaitForSeconds(0.3f);
        }
    }

    private void Update() {
        if (target == null) return;
        if (fireCountdown <= 0) {
            Shoot();
            fireCountdown = 1f / bulletFireRate;
        }
        fireCountdown -= Game.deltaTime;
    }

    void Shoot() {
        GameObject firedBullet = AmmoFactory.ammoFactory.FireBullet(ammoType, target, firePoint, bulletDamage, bulletSpeed, bulletExplosionRadius, effectType);
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, bulletRange);
    }

    public void SetupCircle(Transform towerTransform) {
        towerRangeCircle.transform.localScale = new Vector3(bulletRange*2f, towerRangeCircle.transform.localScale.y, bulletRange*2f);
        towerRangeCircle.transform.position = new Vector3(towerTransform.position.x, 4, towerTransform.position.z);
    }

    public void RemoveCircle() {
        towerRangeCircle.transform.position = new Vector3(towerRangeCircle.transform.position.x, 500, towerRangeCircle.transform.position.z);
    }
}
