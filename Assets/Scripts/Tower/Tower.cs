﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    private TowerBlueprinty towerBlueprinty;
    private GameObject towerGameObject;
    private Vector2 virtualGridLocation;
    private GameObject attachedTile;

    private int towerPower;
    private TowerType towerType = TowerType.NormalHitter;

    private Turret towerTurret;
    [Header("Attributes")]
    private float bulletDamage; // comes from tower level and any power-ups.
    private float bulletSpeed; // Derived from towerType and tower level.
    private float bulletFireRate;
    private float bulletRange;
    private float bulletExplosionRadius;
    private AmmoType towerAmmoType;
    private EffectType towerAmmoEffectType;

// mergePhaseOneTower.GetComponent<Renderer>().sharedMaterial = towerSelectedMaterial;
    void Awake() {
        towerTurret = gameObject.GetComponent<Turret>();
        if (towerTurret == null) {
            Debug.Log("Why is the turret of tower null? TowerLocalPos: " + gameObject.transform.localPosition);
            Destroy(gameObject);
        }
        // Debug.Log("Tower is initalized.");
    }

    private float getTowerPowerModifier(int towerPower) {
        switch(towerPower) {
            case 2: return 0.1f;
            case 4: return 0.2f;
            case 8: return 0.4f;
            case 16: return 0.7f;
            case 32: return 1.1f;
            case 64: return 1.6f;
            case 128: return 2.2f;
            case 256: return 2.9f;
            case 512: return 3.7f;
            case 1024: return 4.6f;
            case 2048: return 5.6f;
            case 4096: return 6.7f;
            default: throw new KeyNotFoundException("TowerPower: " + towerPower + " is invalid!");
        }
    }
    private void adjustTurretStrength() {
        // TODO: Change bullet prefabs when these change.
        switch(towerType) {
            case(TowerType.LightHitter):
                bulletDamage = 10f * getTowerPowerModifier(towerPower);
                bulletSpeed = Mathf.Max(4f, 20f * getTowerPowerModifier(towerPower));
                bulletFireRate = Mathf.Max(2f, 10f * getTowerPowerModifier(towerPower));
                bulletRange = Mathf.Max(5f, 5f * (int) getTowerPowerModifier(towerPower));
                bulletExplosionRadius = 0f;
                break;
            case(TowerType.NormalHitter):
                bulletDamage = 20f * getTowerPowerModifier(towerPower);
                bulletSpeed = 10f * getTowerPowerModifier(towerPower);
                bulletFireRate = 7f * getTowerPowerModifier(towerPower);
                bulletRange = Mathf.Max(10f, 7f * (int) getTowerPowerModifier(towerPower));
                bulletExplosionRadius = 1f * getTowerPowerModifier(towerPower);
                break;
            case(TowerType.HeavyHitter):
                bulletDamage = 35f * getTowerPowerModifier(towerPower);
                bulletSpeed = 3f * getTowerPowerModifier(towerPower);
                bulletFireRate = 1f * getTowerPowerModifier(towerPower);
                bulletRange = Mathf.Max(20f, 10f * (int) getTowerPowerModifier(towerPower));
                bulletExplosionRadius = 3f * getTowerPowerModifier(towerPower);
                break;
        }
        towerTurret.setTurretAttributes(bulletDamage, bulletSpeed, bulletFireRate, bulletRange, bulletExplosionRadius, towerAmmoType, towerAmmoEffectType);
    }

    public void setTowerGameObject(GameObject _towerGameObject) {
        towerGameObject = _towerGameObject;
    }

    public GameObject getTowerGameObject() {
        return towerGameObject;
    }

    public TowerBlueprinty getTowerBlueprinty() {
        return towerBlueprinty;
    }
    public void setTowerBlueprinty(TowerBlueprinty _towerBlueprinty) {
        towerPower = _towerBlueprinty.power;
        towerType = _towerBlueprinty.towerType;
        towerBlueprinty = _towerBlueprinty;
        towerAmmoType = _towerBlueprinty.ammoType;
        towerAmmoEffectType = _towerBlueprinty.ammoEffectType;
        adjustTurretStrength();
    }
    public void setTowerType(TowerType _towerType) {
        towerType = _towerType;
        adjustTurretStrength();
    }
    public TowerType getTowerType() {
        return towerType;
    }
    public void setTowerPower(int _towerPower) {
        towerPower = _towerPower;
        adjustTurretStrength();
    }
    public int getTowerPower() {
        return towerPower;
    }
    public Vector2 getTowerVirtualGridLocation() {
        return virtualGridLocation;
    }
    public void setTowerVirtualGridLocation(Vector2 pos) {
        virtualGridLocation = pos;
    }
    public void setAttachedTile(GameObject tile) {
        attachedTile = tile;
    }
    public GameObject getAttachedTile() {
        return attachedTile;
    }
    public string getTowerName() {
        return "Tower " + towerBlueprinty.power;
    }
    public float getTowerRange() {
        return bulletRange;
    }
    public float getTowerDamage() {
        return bulletDamage;
    }
    public AmmoType getAmmoType() {
        return towerAmmoType;
    }

    public int GetPurchasePrice()
    {
        return towerBlueprinty.GetPurchasePrice();
    }
    
    public int GetSellPrice()
    {
        return towerBlueprinty.GetSellPrice();
    }

    public void DrawTowerRangeCircle() {
        towerTurret.GetComponent<Turret>().SetupCircle(transform);
    }
    public void DeleteTowerRangeCircle() {
        towerTurret.GetComponent<Turret>().RemoveCircle();
    }
}