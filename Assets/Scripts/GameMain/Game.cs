using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameMain
{
    public class Game : MonoBehaviour {
        public static Game game;

        public int maxBetRaidLevel;
        public float easyDifficultyBetMultiplier;
        public float normalDifficultyBetMultiplier;
        public float hardDifficultyBetMultiplier;

        public ShowGameSpeedUI gameSpeedUI;
        public IngameOverlayCanvasController ingameCanvas;

        public GameObject UIBlurPanel;

        public MainMenuUIController mainMenuUIController;

        private static bool isPaused = false;
        public static float deltaTime => isPaused ? 0 : Time.deltaTime;

        public static int currentTimescaleIdx;
        private static List<float> allowableTimescaleList = new List<float>(){1, 2, 10};

        public static Difficulty gameDifficulty;

        void Awake() {
            game = this;
            currentTimescaleIdx = 0;
        }

        void Start() {
            gameDifficulty = Difficulty.Hard;
            UIActivate();
        }

        private void enableObjectsToStart() {
            ingameCanvas.InitRound();
        }

        private CoroutineHandle disableObjectsToEnd() {
            return Timing.RunCoroutine(ingameCanvas.EndRound());
        }

        private void startPlaying(bool restartBoard = true) {
            Debug.Log("Will start playing in difficulty: " + gameDifficulty);
            UIDeactivate();
            enableObjectsToStart();
            if (restartBoard) {
                Debug.Log("Will restart the board");
                GameBoard.gameBoard.RestartBoard();
            }
            GameBoard.gameBoard.InitRound();
            Player.player.InitRound(BetRaidPredictionsController.betRaidPredictionsController.GetRaidBetAmount());
        }

        public void StartPlayingModeEasy() {
            bool restartBoard = false;
            if (gameDifficulty != Difficulty.Easy) {
                gameDifficulty = Difficulty.Easy;
                restartBoard = true;
            }
            startPlaying(restartBoard);
        }

        public void StartPlayingModeMedium() {
            bool restartBoard = false;
            if (gameDifficulty != Difficulty.Normal) {
                gameDifficulty = Difficulty.Normal;
                restartBoard = true;
            }
            startPlaying(restartBoard);
        }

        public void StartPlayingModeHard() {
            bool restartBoard = false;
            if (gameDifficulty != Difficulty.Hard) {
                gameDifficulty = Difficulty.Hard;
                restartBoard = true;
            }
            startPlaying(restartBoard);
        }

        public IEnumerator<float> GameOver() {
            SetGamespeed(1f);
            UIActivate();
            yield return Timing.WaitUntilDone(disableObjectsToEnd());
            int betEarntMoney = 
                BetRaidPredictionsController.betRaidPredictionsController.EndRound(WaveSpawner.GetLastFinishedWave());
            Player.player.EndRound(betEarntMoney);
            yield return Timing.WaitUntilDone(mainMenuUIController.GameOver());
            WaveSpawner.waveSpawner.EndRound();
            EnemyFactory.enemyFactory.EndRound();
            AmmoFactory.ammoFactory.EndRound();
            GameBoard.gameBoard.EndRound();
            yield return Timing.WaitForOneFrame;
        }

        public void MainMenu() {

        }

        public void UIActivate() {
            TowerController.towerController.DisableClickingOnTowers();
            Pause();
            UIBlurPanel.SetActive(true);
        }

        public void UIDeactivate() {
            TowerController.towerController.EnableClickingOnTowers();
            Unpause();
            UIBlurPanel.SetActive(false);
        }

        public static bool IsPaused() {
            return isPaused;
        }

        public static void Pause() {
            isPaused = true;
        }

        public static void Unpause() {
            isPaused = false;
        }

        public void ToggleGamespeed() {
            currentTimescaleIdx = (currentTimescaleIdx + 1) % allowableTimescaleList.Count;
            SetGamespeed(allowableTimescaleList[currentTimescaleIdx]);
        }

        private void SetGamespeed(float gamespeed) {
            Time.timeScale = gamespeed;
            Debug.Log("Time scale text changed.");
            gameSpeedUI.gameSpeedText.text = Time.timeScale.ToString() + "x";
        }

        public float GetDifficultyMultiplier()
        {
            float difficultyMultiplier;
            switch (gameDifficulty)
            {
                case Difficulty.Easy: 
                    difficultyMultiplier = easyDifficultyBetMultiplier;
                    break;
                case Difficulty.Normal: 
                    difficultyMultiplier = normalDifficultyBetMultiplier;
                    break;
                case Difficulty.Hard: 
                    difficultyMultiplier = hardDifficultyBetMultiplier;
                    break;
                default:  
                    difficultyMultiplier = easyDifficultyBetMultiplier;
                    break;
            }
            return difficultyMultiplier;
        }
    }
}