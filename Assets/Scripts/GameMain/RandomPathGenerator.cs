﻿using System.Collections.Generic;
using Priority_Queue;
using UnityEngine;

public class RandomPathGenerator : MonoBehaviour
{
    private Vector2Int[] availableGridDirections = new Vector2Int[4] {Vector2Int.right, Vector2Int.up, Vector2Int.down, Vector2Int.left};

    public Dictionary<PathNode, PathNode> findPathToCreate(GameObject pathTile, PathNode startingNode, PathNode endingNode, GameObject tileType1, GameObject tileType2) {
        SimplePriorityQueue<PathNode> frontier = new SimplePriorityQueue<PathNode>();
        Dictionary<PathNode, PathNode> came_from = new Dictionary<PathNode, PathNode>();
        Dictionary<PathNode, float> cost_so_far = new Dictionary<PathNode, float>();

        GameObject emptyNodeIndicator = tileType1;
        PathNode current = startingNode;
        float priority = 0;
        while (current.pos != endingNode.pos) {
            List<List<GameObject>> visitedGridNodes = new List<List<GameObject>>();
            foreach (List<GameObject> row in GameBoard.gameBoard.virtualGrid) {
                List<GameObject> newList = new List<GameObject>();
                foreach (GameObject _ in row) {
                    newList.Add(null);
                }
                visitedGridNodes.Add(newList);
            }

            generateMaze(visitedGridNodes, startingNode.pos, endingNode.pos, emptyNodeIndicator: emptyNodeIndicator);
            
            frontier.Clear();
            cost_so_far.Clear();
            came_from.Clear();

            frontier.Enqueue(startingNode, priority);
            came_from[startingNode] = null;
            cost_so_far[startingNode] = 0;
            while (frontier.Count != 0) {
                current = frontier.Dequeue();
                if (current.pos == endingNode.pos) { 
                    came_from[endingNode] = current;
                    break;
                }
                foreach (Vector2Int nextNodePos in graphNeighborsOfCurrent(visitedGridNodes, current.pos, emptyNodeIndicator: emptyNodeIndicator, endingPos: endingNode.pos)) {
                    float new_cost = cost_so_far[current] + 1;
                    PathNode nextNode = new PathNode(nextNodePos);
                    if (!cost_so_far.ContainsKey(nextNode) || new_cost < cost_so_far[nextNode]) {
                        cost_so_far[nextNode] = new_cost;
                        priority = new_cost + Mathf.Abs(endingNode.pos.x-nextNode.pos.x) + Mathf.Abs(endingNode.pos.y - nextNode.pos.y);
                        frontier.Enqueue(nextNode, priority);
                        came_from[nextNode] = current;
                        visitedGridNodes[nextNodePos.x][nextNodePos.y] = tileType2;
                    }
                }
            }
        }
        return came_from;
    }

    private void generateMaze(List<List<GameObject>> visitedGridNodes, Vector2Int startPos, Vector2Int endingPos, GameObject emptyNodeIndicator) {    
        Stack<Vector2Int> stack = new Stack<Vector2Int>();
        List<Vector2Int> came_from = new List<Vector2Int>();

        Vector2Int current = startPos, neighborNode, wallBetweenNeighborToBreak;
        Vector2Int endingPosPrev = new Vector2Int(endingPos.x, endingPos.y-1);

        stack.Push(startPos);
        visitedGridNodes[startPos.x][startPos.y] = emptyNodeIndicator;

        came_from.Add(endingPos);
        visitedGridNodes[endingPos.x][endingPos.y] = emptyNodeIndicator;
        came_from.Add(endingPosPrev);
        visitedGridNodes[endingPosPrev.x][endingPosPrev.y] = emptyNodeIndicator;
        
        while (stack.Count > 0) { 
            current = stack.Pop();
            List<Vector2Int> neighbors = graphNeighborsOfCurrent(visitedGridNodes, current, null, 2);
            if (neighbors.Count > 0) {
                stack.Push(current);
                neighborNode = neighbors[Random.Range(0, neighbors.Count)];
                stack.Push(neighborNode);

                came_from.Add(current);
                visitedGridNodes[current.x][current.y] = emptyNodeIndicator;
                wallBetweenNeighborToBreak = new Vector2Int((current.x + neighborNode.x)/2, (current.y + neighborNode.y)/2);

                came_from.Add(wallBetweenNeighborToBreak);
                visitedGridNodes[wallBetweenNeighborToBreak.x][wallBetweenNeighborToBreak.y] = emptyNodeIndicator;

                came_from.Add(neighborNode);
                visitedGridNodes[neighborNode.x][neighborNode.y] = emptyNodeIndicator;
            }
        }
    }

    private List<Vector2Int> graphNeighborsOfCurrent(List<List<GameObject>> visitedGridNodes, Vector2Int pos, GameObject emptyNodeIndicator, int neighborDistance = 1, Vector2Int? endingPos = null) {
        List<Vector2Int> neighborNodes = new List<Vector2Int>();
        Vector2Int directionPos;
        foreach (Vector2Int direction in availableGridDirections) {
            directionPos = pos + direction * neighborDistance;
            if (endingPos.HasValue && directionPos == endingPos) {
                neighborNodes.Add(directionPos);
                return neighborNodes;
            }
            if (directionPos.x >= GameBoard.gameBoard.gridWidth-1 || directionPos.x <= 0 || directionPos.y >= GameBoard.gameBoard.gridHeight-1 || directionPos.y <= 0)
                continue;
            if (visitedGridNodes[directionPos.x][directionPos.y] != emptyNodeIndicator) {
                continue;
            }
            neighborNodes.Add(directionPos);
        }
        return neighborNodes;
    }
}

public class PathNode : FastPriorityQueueNode {
    public Vector2Int pos { get; set; }
    public PathNode(Vector2Int pos) {
        this.pos = pos;
    }
}
