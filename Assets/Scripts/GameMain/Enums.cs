using System.Collections.Generic;

public enum Difficulty
{
    Easy = 3,
    Normal = 6,
    Hard = 10
}

public enum MergePhase {
    StartingPhase,
    FirstTowerSelected,
    SecondTowerSelected,
    FirstTowerMovedToSecondTower,
    BuildNewTower
}

public enum TowerType {
  LightHitter,
  NormalHitter,
  HeavyHitter
}

public enum TowerPower {
    _2=2,
    _4=4,
    _8=8,
    _16=16,
    _32=32,
    _64=64,
    _128=128,
    _256=256,
    _512=512,
    _1024=1024,
    _2048=2048
}

static class EnumLookupTables {
    public static Dictionary<TowerType, string> towerTypeLookup = new Dictionary<TowerType, string> {
        {TowerType.LightHitter, "Light Attacker"},
        {TowerType.NormalHitter, "Regular Attacker"},
        {TowerType.HeavyHitter, "Heavy Attacker"}
    };

    public static Dictionary<AmmoType, string> ammoTypeLookup = new Dictionary<AmmoType, string> {
        {AmmoType.Bullet, "Regular"},
        {AmmoType.Missile, "Missile"}
    };
}

public enum AmmoType {
    Bullet = 0,
    Missile = 1
}