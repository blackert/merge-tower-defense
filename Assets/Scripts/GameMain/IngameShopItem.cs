﻿using TMPro;
using UnityEngine;

public class IngameShopItem : MonoBehaviour
{
    public bool isPowerup = false;
    public bool isTower = true;
    public TowerType towerType;
    public TowerPower towerPower;
    private TowerBlueprinty towerBlueprinty;
    private int towerPrice;
    private int towerIndex;

    void Start()
    {
        // powerupController = PowerupController.powerupController;
        towerIndex = (int) Mathf.Log((int)towerPower, 2) - 1;
        towerBlueprinty = TowerController.towerController.GetTowerBlueprintyByType(towerIndex, towerType);
        towerPrice = towerBlueprinty.GetPurchasePrice();

        setTowerPriceInUI();
    }

    public bool Bought() {
        if (isTower) {
            return purchaseTower(towerBlueprinty, towerPrice);
        } else if (isPowerup) {
            Debug.Log("Powerup purchase is not yet implemented.");
            return false;
        }
        return false;
    }

    private bool purchaseTower(TowerBlueprinty tower, int cost) {
        if (Player.player.canBuyItem(cost)) {
            Player.player.boughtItem(cost);
            TowerController.towerController.ShopTower(tower);
            return true;
        }
        else {
            Debug.Log("Cannot purchase this tower!");
            return false;
            // TODO: Show message to player. Insufficient funds to purchase item.
        }
    }

    private void setTowerPriceInUI() {
        foreach (Transform child in transform)
        {
            if(child.name == "TowerPrice") {
                child.GetComponent<TextMeshProUGUI>().text = "Price: " + towerPrice;
            }
        }
    }

    private void purchasePowerup() {
        throw new System.NotImplementedException();
    }
}
