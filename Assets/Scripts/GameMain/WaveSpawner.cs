﻿using System.Collections.Generic;
using GameMain;
using UnityEngine;

using MEC;
using TMPro;
using UnityEngine.Serialization;

public class WaveSpawner : MonoBehaviour
{
    public static WaveSpawner waveSpawner;

    public float waitSecondsAfterEnemiesSpawn = 15;
    private float initialWaveLength;
    public float countdownToNextWave;
    public TextMeshProUGUI waveCountdownText;
    public TextMeshProUGUI waveNumberText;
    public TextMeshProUGUI completedWaveNumberText;

    public static int waveCount = 1;
    public static int waveCompleted = 0;
    private static Dictionary<int, int> waveRemainingEnemyMap;
    public int switchEnemiesInRounds = 10;
    [SerializeField] private int numOfEnemiesInThisWave;
    
    private bool waitCountdownToNextWave = false;
    private bool sendEnemiesNow = false;
    private bool waveSpawnActive = false;
    private Queue<int> waveSpawnQueue;
    private int lastQueuedWave;

    // [SerializeField] public static List<GameObject> enemyCache;
    // Update is called once per frame

    void Awake() {
        waveSpawner = this;
        waveRemainingEnemyMap = new Dictionary<int, int>();
    }

    void Start()
    {
        initialWaveLength = waitSecondsAfterEnemiesSpawn;
        InitRound();
    }

    public void InitRound() {
        countdownToNextWave = waitSecondsAfterEnemiesSpawn;
        waitCountdownToNextWave = true;
        waveCount = 0;
        waveCompleted = 0;
        waveSpawnActive = false;
        waveSpawnQueue = new Queue<int>();
        lastQueuedWave = 0;
        waveRemainingEnemyMap.Clear();
        waveRemainingEnemyMap[waveCount] = 0;
        waveNumberText.text = waveCount.ToString("000");
        completedWaveNumberText.text = waveCompleted.ToString("000");
        waveCountdownText.text = Mathf.FloorToInt(countdownToNextWave).ToString("00");
    }

    public void EndRound() {
        waitCountdownToNextWave = true;
    }

    public static int GetLastFinishedWave()
    {
        return waveCount;
    }

    void LateUpdate()
    {
        if (sendEnemiesNow || (waveSpawnQueue.Count > 0 && !waveSpawnActive))
        {
            if (waveSpawnActive)
            {
                waveSpawnQueue.Enqueue(++lastQueuedWave);
                sendEnemiesNow = false;
            }
            else
            {
                if (waveSpawnQueue.Count > 0)
                    waveCount = waveSpawnQueue.Dequeue();
                else
                {
                    ++waveCount;
                    lastQueuedWave = waveCount;
                }

                Timing.RunCoroutine(_spawnWave(waveCount));
                countdownToNextWave = waitSecondsAfterEnemiesSpawn;
                sendEnemiesNow = false;
            }
        }

        if (countdownToNextWave <= 0f)
            sendEnemiesNow = true;
        
        waveCountdownText.text = Mathf.FloorToInt(countdownToNextWave).ToString("00");
        if (!waitCountdownToNextWave)
            countdownToNextWave -= Game.deltaTime;
    }

    public void gameBoardSetupComplete() {
        Debug.Log("Game board complete.");
        EnemyFactory.enemyFactory.InitRound();
        InitRound();
        waitCountdownToNextWave = false;
    }

    public void SendEnemiesNow() {
        // Used to `skip` waves from the UI.
        sendEnemiesNow = true;
    }

    private IEnumerator<float> _spawnWave(int waveCount)
    {
        waveSpawnActive = true;
        // TODO: Active wave
        bool bossWave = false;
        waveNumberText.text = waveCount.ToString("000");
        Debug.Log("Wave " + waveCount + " is spawning!");
        waitCountdownToNextWave = true;
        numOfEnemiesInThisWave = Mathf.Clamp((int)Mathf.Pow(waveCount, 1.5f), 1, 100);
        waitSecondsAfterEnemiesSpawn = Mathf.Clamp(++waitSecondsAfterEnemiesSpawn, 15, 55);
        switch (waveCount % switchEnemiesInRounds)
        {
            case 0:
                Debug.Log("Spawning BOSS ROUND!!");
                Mathf.Max(1, numOfEnemiesInThisWave /= 10);
                bossWave = true;
                break;
            case 1 when waveCount != 1: // Boss at 10th levels, 11 level switches enemy.
                EnemyFactory.enemyFactory.SwitchEnemyType();
                yield return Timing.WaitForSeconds(1f);  // To allow the cache to populate some enemies.
                break;
        }

        waveRemainingEnemyMap[waveCount] = numOfEnemiesInThisWave;
        EnemyFactory.enemyFactory.InitWave(bossWave:bossWave, waveNumber:waveCount);
        for (var i = 0; i < numOfEnemiesInThisWave; i++) {
            spawnEnemy();
            yield return Timing.WaitForSeconds(1f);
        }
        waitCountdownToNextWave = false;
        EnemyFactory.enemyFactory.EndWave();
        waveSpawnActive = false;
    }

    private void spawnEnemy() {
        EnemyFactory.enemyFactory.GetEnemy();
    }

    public void EnemyDead(int enemyInWave)
    {
        waveRemainingEnemyMap[enemyInWave]--;
        if (waveRemainingEnemyMap[enemyInWave] < 0)
        {
            Debug.Log("More enemy killed than generated? This should not happen.");
        }

        if (waveRemainingEnemyMap[enemyInWave] == 0)
        {
            waveCompleted = enemyInWave;
            completedWaveNumberText.text = waveCompleted.ToString("000");
            Debug.Log("Completed wave: " + waveCompleted);
        }
    }

}
