﻿using System.Collections.Generic;
using GameMain;
using UnityEngine;

using TMPro;
using MEC;

public class Player : MonoBehaviour
{
    public static Player player;

    public TextMeshProUGUI healthText;
    public TextMeshProUGUI moneyText;
    public int health = 1000;
    private int _roundHealth;
    public int money;
    private int _roundMoney;

    private int enemiesKilled;
    private int bossesKilled;

    [SerializeField]
    private int stashMoney = 10000;  // TESTING. TODO

    private int earnedMoneyInLastRound;
    private int totalWorthInLastRound;

    void Awake() {
        player = this;
    }

    void Start() {
        InitRound(0);
    }

    public void InitRound(int bettedMoney) {
        _roundHealth = health;
        _roundMoney = money;
        enemiesKilled = 0;
        bossesKilled = 0;
        earnedMoneyInLastRound = 0;
        totalWorthInLastRound = 0;

        stashMoney -= bettedMoney;
        _roundMoney += CalculateBetMoneyToIngameConversion(bettedMoney);
        
        healthChanged();
        moneyChanged();
    }

    private int CalculateBetMoneyToIngameConversion(int bettedAmount)
    {
        Debug.Log(Game.game.GetDifficultyMultiplier()*5*bettedAmount);
        return (int) Mathf.Max(bettedAmount-(Game.game.GetDifficultyMultiplier()*5*bettedAmount), 0);
    }

    public void EndRound(int betEarntMoney) {
        stashMoney += betEarntMoney;
        earnedMoneyInLastRound += betEarntMoney;
        if (totalWorthInLastRound <= 0)
        {
            GetNetWorthInLastRound();
        }
        float difficultyMultiplier = Game.game.GetDifficultyMultiplier();
        earnedMoneyInLastRound += (int)(totalWorthInLastRound * difficultyMultiplier);
        Debug.Log("Money earned in last round: " + (int)(totalWorthInLastRound * difficultyMultiplier));
        
        // InitRound();
    }

    public int GetMoneyEarnedLastRound()
    {
        return earnedMoneyInLastRound;
    }

    public void enemyKilled(int reward, bool boss=false) {
        if (boss) {
            bossesKilled++;
        } else {
            enemiesKilled++;
        }
        _roundMoney += reward;
        moneyChanged();
    }

    public void decreaseHealth(int decrease = 1) {
        _roundHealth -= decrease;
        healthChanged();
    }

    public void increaseHealth(int increase = 1) {
        _roundHealth += increase;
        healthChanged();
    }

    private void healthChanged() {
        healthText.text = _roundHealth.ToString("000");
        if (_roundHealth <= 0) {
            Timing.RunCoroutine(Game.game.GameOver());
        }
    }

    public bool canBuyItem(int cost) {
        return _roundMoney >= cost;
    }

    public void boughtItem(int cost) {
        Debug.Log("Bought item with cost: " + cost);
        _roundMoney -= cost;
        moneyChanged();
    }

    public void refundItem(int cost) {
        _roundMoney += cost;
        moneyChanged();
    }

    public void soldItem(int cost) {
        _roundMoney += cost;
        moneyChanged();
    }

    private void moneyChanged() {
        moneyText.text = _roundMoney.ToString();
    }

    public int GetNumberOfEnemiesKilled() {
        return enemiesKilled;
    }

    public int GetNumberOfBossesKilled() {
        return bossesKilled;
    }

    public int GetNetWorthInLastRound()
    {
        List<Tower> towerList = GameBoard.gameBoard.GetListOfAllCreatedTowers();
        foreach (var tower in towerList)
        {
            totalWorthInLastRound += tower.GetPurchasePrice();
        }
        return totalWorthInLastRound;
    }

    public int GetStashMoney()
    {
        return stashMoney;
    }

}
