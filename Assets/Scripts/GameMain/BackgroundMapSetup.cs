﻿using System.Collections.Generic;
using UnityEngine;

using MEC;

public class BackgroundMapSetup : MonoBehaviour
{

    public GameObject backgroundObjectPrefab;
    public Transform backgroundObjectParent;
    public int xMin, xMax, zMin, zMax, y;  // of the background.
    public int gameBoardXMin, gameBoardXMax, gameBoardZMin, gameBoardZMax;

    public int randomness = 2;
    public int horizontalDististBetweenBackgroundObjects = 10, verticalDististBetweenBackgroundObjects = 5;

    void Start()
    {
        // Timing.RunCoroutine(_setupGameBackground());
    }

    void instantiateBackgroundObject(int x, int y, int z) {
        if (x > gameBoardXMin && x < gameBoardXMax && z > gameBoardZMin && z < gameBoardZMax) return;
        Instantiate(backgroundObjectPrefab, new Vector3(x, y, z), Quaternion.identity, backgroundObjectParent);
    }

    private IEnumerator<float> _setupGameBackground() {
        // Start from upper left corner. (highest horizontal and vertical distance to the map)
        for (int x = xMin; x < xMax; x += horizontalDististBetweenBackgroundObjects + Random.Range(-randomness, randomness+1)) {
            for (int z = zMin; z < zMax; z += verticalDististBetweenBackgroundObjects  + Random.Range(-randomness, randomness+1)) {
                instantiateBackgroundObject(x + Random.Range(-randomness, randomness), y + Random.Range(-randomness, randomness), z + Random.Range(-randomness, randomness));
            }
            yield return Timing.WaitForOneFrame;
        }
    }
}
