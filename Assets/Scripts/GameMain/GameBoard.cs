﻿using System.Collections.Generic;
using GameMain;
using UnityEngine;

using MEC;

public class GameBoard : MonoBehaviour {
    public static GameBoard gameBoard;

    public List<GameObject> grasses;
    public List<GameObject> boundary;
    public List<GameObject> destroyableObjects;
    public List<GameObject> trees;
    public List<GameObject> rocks;
    public List<GameObject> paths;

    public bool randomizeMapGeneration = true;
    private int randomGameBoardGeneratorSeed = 1;

    private float destroyableObjectGenerationProbabilty = 0.2f;

    public int easyStartingTowerCount = 4;
    public int hardStartingTowerCount = 3;

    public GameObject friendTiles;
    public GameObject enemyPathTiles;
    public GameObject neutralTiles;
    public GameObject placedTowers;
    public float tower4InitializationProbability = 0.1f;
    private GameObject selectedGrass;
    private static GameObject selectedBoundary;
    private GameObject selectedPath;
    public float tileScale = 5;

    private List<List<Vector3>> gridToRealWorld = new List<List<Vector3>>();
    [HideInInspector] public List<List<GameObject>> virtualGrid = new List<List<GameObject>>();
    [HideInInspector] public int gridWidth, gridHeight;
    private int enemyPathStartingPos, enemyPathEndingPos;
    private List<GameObject> playableTiles = new List<GameObject>();
    private List<Vector2> playableTilesVirtualPos = new List<Vector2>();
    private List<GameObject> pathTiles = new List<GameObject>(); // From start to end;

    private bool needToReSetupAfterGameOver;
    private bool gameBoardSetupComplete;

    void Start() {
        gameBoard = this;
        placedTowers = GameObject.Find("Towers");
        InitGameBoard();
    }

    public void InitGameBoard() {
        gameBoardSetupComplete = false;
        needToReSetupAfterGameOver = false;
        Debug.Log("Init game board.");
        RandomlySelectTextures();
        GridifyThePlane();
        CoroutineHandle setupGameBoardHandle = Timing.RunCoroutine(_setupRandomGameBoard());
        Timing.RunCoroutine(_setupInitialTowers(setupGameBoardHandle));
    }

    public void InitRound() {
        if (needToReSetupAfterGameOver)
        {
            Debug.Log("After restart, need to re-setup the board.");
            RestartBoard();
        }
        Debug.Log("Init wave spawner.");
        Timing.RunCoroutine(_gameBoardSetupCompleteStartWaveSpawner());
    }

    public void EndRound()
    {
        needToReSetupAfterGameOver = true;
    }

    public void RestartBoard()
    {
        gameBoardSetupComplete = false;
        needToReSetupAfterGameOver = false;
        Timing.KillCoroutines();
        Timing.RunCoroutine(_RestartBoard());
    }

    private IEnumerator<float> _RestartBoard() {
        gridToRealWorld.Clear();
        gridToRealWorld = new List<List<Vector3>>();

        yield return Timing.WaitUntilDone(Timing.RunCoroutine(ClearCreatedObjects()));
        playableTiles = new List<GameObject>();
        playableTilesVirtualPos = new List<Vector2>();
        pathTiles = new List<GameObject>();
        virtualGrid = new List<List<GameObject>>();

        InitGameBoard();
    }
    
    private IEnumerator<float> ClearCreatedObjects() {
        // foreach (GameObject obj in playableTiles) {
        //     Debug.Log("k:" + k++);
        //     Destroy(obj);
        // }
        playableTiles.Clear();
        playableTilesVirtualPos.Clear();
        pathTiles.Clear();
        virtualGrid.Clear();
        // foreach (GameObject obj in pathTiles) {
        //     Destroy(obj);
        // }
        
        // foreach (List<GameObject> listOfObj in virtualGrid) {
        //     foreach (GameObject obj in listOfObj) {
        //         Destroy(obj);
        //     }
        // }

        for(int i = 0; i < enemyPathTiles.transform.childCount; i++) {
            Destroy(enemyPathTiles.transform.GetChild(i).gameObject);
        }
        yield return Timing.WaitForOneFrame;
        for(int i = 0; i < neutralTiles.transform.childCount; i++) {
            Destroy(neutralTiles.transform.GetChild(i).gameObject);
        }
        yield return Timing.WaitForOneFrame;
        for(int i = 0; i < friendTiles.transform.childCount; i++) {
            Destroy(friendTiles.transform.GetChild(i).gameObject);
        }
        yield return Timing.WaitForOneFrame;
        for(int i = 0; i < placedTowers.transform.childCount; i++) {
            Destroy(placedTowers.transform.GetChild(i).gameObject);
        }
    }

    private void RandomlySelectTextures() {
        if (randomizeMapGeneration) {
            randomGameBoardGeneratorSeed = (int)System.DateTime.Now.Ticks;
            Debug.Log("Random game seed: " + randomGameBoardGeneratorSeed.ToString());
        }
        Random.InitState(randomGameBoardGeneratorSeed);

        selectedGrass = grasses[Random.Range(0, grasses.Count)];
        selectedBoundary = boundary[Random.Range(0, boundary.Count)];
        selectedPath = paths[Random.Range(0, paths.Count)];
    }

    private void GridifyThePlane() {
        Bounds planeMeshBounds = GetComponent<MeshFilter>().mesh.bounds;

        Vector3 planeMeshMinToWorld = transform.TransformPoint(planeMeshBounds.min);
        Vector3 planeMeshSizeToWorld = transform.TransformPoint(planeMeshBounds.size);
        gridWidth = (int) (planeMeshSizeToWorld.x / tileScale) + 1;
        gridHeight = (int) (planeMeshSizeToWorld.z / tileScale) + 1;
        float curXPos = planeMeshMinToWorld.x, curYPos = planeMeshMinToWorld.y, curZPos = planeMeshMinToWorld.z;

        for(int i = 0; i < gridWidth; i++) {
            gridToRealWorld.Add(new List<Vector3>());
            virtualGrid.Add(new List<GameObject>());
            for(int j = 0; j < gridHeight; j++) {
                gridToRealWorld[i].Add(new Vector3(curXPos, curYPos, curZPos));
                virtualGrid[i].Add(null);
                curZPos += tileScale;
            }
            curXPos += tileScale;
            curZPos = planeMeshMinToWorld.z;
        }
    }

    public void CreateNewTile(GameObject prefab, int x, int y, bool? isUserPlayable=true) {
        GameObject tile;
        if (isUserPlayable.Equals(true)) {
            tile = (GameObject) Instantiate(prefab, gridToRealWorld[x][y], Quaternion.identity, friendTiles.transform);
            playableTiles.Add(tile);
            playableTilesVirtualPos.Add(new Vector2(x, y));
        } else if (isUserPlayable == null) {
            tile = (GameObject) Instantiate(prefab, gridToRealWorld[x][y], Quaternion.identity, neutralTiles.transform);
        } else {
            tile = (GameObject) Instantiate(prefab, gridToRealWorld[x][y], Quaternion.identity, enemyPathTiles.transform);
            pathTiles.Add(tile);
        }
        virtualGrid[x][y] = tile;
    }

    public GameObject CreateNewTower(TowerBlueprinty towerBlueprinty, Vector2 virtualPos) {
        Vector3 pos = gridToRealWorld[(int)virtualPos.x][(int)virtualPos.y];
        pos.y = TowerController.GETTowerDefaultYPos(towerBlueprinty);
        GameObject createdTower = CreateTower(towerBlueprinty, pos);
        AddTowerToTile(createdTower, virtualPos);
        return createdTower;
    }
    public GameObject CreateTower(TowerBlueprinty towerBlueprinty, Vector3 pos) {
        GameObject createdTower = (GameObject) Instantiate(towerBlueprinty.towerPrefab, pos, Quaternion.identity, placedTowers.transform);
        Tower script = createdTower.GetComponent<Tower>();
        script.setTowerGameObject(createdTower);
        script.setTowerBlueprinty(towerBlueprinty);
        return createdTower;
    }

    public List<Tower> GetListOfAllCreatedTowers()
    {
        List<Tower> towerList = new List<Tower>();
        foreach (Transform tower in placedTowers.transform)
        {
            towerList.Add(tower.GetComponent<Tower>());
        }

        return towerList;
    }
    
    public void RemoveTowerFromTile(GameObject tower) {
        Tower script = tower.GetComponent<Tower>();
        if (script.getAttachedTile() != null) {
            playableTiles.Add(script.getAttachedTile());
        } else Debug.Log("This should not happen! Tower has no tile attached. Tower:" + tower + " - " + tower.transform.position);
        if (script.getTowerVirtualGridLocation() != null) {
            playableTilesVirtualPos.Add(script.getTowerVirtualGridLocation());
        } else Debug.Log("This should not happen! Tower has no VirtualGridLocation attached. Tower:" + tower + " - " + tower.transform.position);
        script.setAttachedTile(null);
        script.setTowerVirtualGridLocation(Vector2.zero);
    }
    public void AddTowerToTile(GameObject tower, Vector2 targetGridLocation) {
        Tower script = tower.GetComponent<Tower>();
        GameObject newTile = virtualGrid[(int)targetGridLocation.x][(int)targetGridLocation.y];

        int tileIdx = playableTiles.IndexOf(newTile);
        if (tileIdx != -1) {
            script.setAttachedTile(newTile);
            script.setTowerVirtualGridLocation(targetGridLocation);
            playableTiles.RemoveAt(tileIdx);
            playableTilesVirtualPos.RemoveAt(tileIdx);
        }
        else {
            Debug.Log("Cannot add tower to tile because it is not playable! tower:" + tower.transform.position + " - targetTile: " + newTile.transform.position);
        }
    }
    public void RelocateTowerToTile(GameObject tower, Vector2 targetGridLocation) {
        RemoveTowerFromTile(tower);
        AddTowerToTile(tower, targetGridLocation);
    }
    public bool IsVirtualLocationPlayable(Vector2 targetGridLocation) {
        GameObject tile = virtualGrid[(int)targetGridLocation.x][(int)targetGridLocation.y];
        if (playableTiles.Contains(tile)) {
            return true;
        }
        return false;
    }

    public Vector3 getNextPath(int prevPathIdx) {
        if (prevPathIdx < 0 || prevPathIdx >= pathTiles.Count) {
            return Vector3.zero;
        }
        return pathTiles[prevPathIdx].transform.position;
    }

    private IEnumerator<float> _setupRandomGameBoard() {
        enemyPathStartingPos = 1; //Random.Range(1, gridWidth - 1);
        enemyPathEndingPos = gridWidth - 2; //Random.Range(1, gridWidth - 1);

        CoroutineHandle borderTilesCoHandle = Timing.RunCoroutine(_placeUnplayableTilesOnTheBorder(selectedBoundary));
        CoroutineHandle pathTilesCoHandle = Timing.RunCoroutine(_placePaths(selectedPath));
        yield return Timing.WaitUntilDone(borderTilesCoHandle);
        yield return Timing.WaitUntilDone(pathTilesCoHandle);
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(_placeUserPlayableTiles(new List<GameObject> {selectedGrass})));
    }

    private IEnumerator<float> _placeUnplayableTilesOnTheBorder(GameObject tile) {
        int x = 0, y;
        foreach (List<Vector3> row in gridToRealWorld) {
            if (x == 0 || x == gridWidth-1) {
                y = 0;
                foreach(Vector3 gridLocation in row) {
                    CreateNewTile(tile, x, y, null);
                    CreateNewTile(rocks[Random.Range(0, rocks.Count)], x, y, null);
                    y++;
                    yield return Timing.WaitForOneFrame;
                } 
            } else {
                if (x != enemyPathStartingPos) {
                    CreateNewTile(tile, x, 0, null);
                    CreateNewTile(rocks[Random.Range(0, rocks.Count)], x, 0, null);
                }
                if (x != enemyPathEndingPos) {
                    CreateNewTile(tile, x, gridHeight-1, null);
                    CreateNewTile(rocks[Random.Range(0, rocks.Count)], x, gridHeight-1, null);
                }
                yield return Timing.WaitForOneFrame;
            }
            x++;
            // yield return Timing.WaitForOneFrame;
        }
        Debug.Log("Border tile placement is finished.");
    }

    //
    // To be run before placing any other tile. Except border tiles.
    //
    private IEnumerator<float> _placePaths(GameObject pathTile) {
        Vector2Int startingGridLoc = new Vector2Int(enemyPathStartingPos, 0);
        Vector2Int endingGridLoc = new Vector2Int(enemyPathEndingPos, gridHeight - 1);
        PathNode start = new PathNode(startingGridLoc), end = new PathNode(endingGridLoc), current = end;
        Debug.Log("In place paths!");
        RandomPathGenerator pathGenerator = GetComponent<RandomPathGenerator>();

        Dictionary<PathNode, PathNode> cameFrom;
        while (true) {
            yield return Timing.WaitForOneFrame;
            int pathLength = 0;
            cameFrom = pathGenerator.findPathToCreate(pathTile, start, end, selectedGrass, selectedPath);
            current = end;
            while (current != start) {
                current = cameFrom[current];
                pathLength++;
            }
            if (pathLengthOkForDifficulty(pathLength) == true) {
                break;
            }
            cameFrom.Clear();
        }
        current = end;
        while (current != start) {
            CreateNewTile(pathTile, current.pos.x, current.pos.y, false);
            current = cameFrom[current];
            yield return Timing.WaitForOneFrame;
        }
        CreateNewTile(pathTile, startingGridLoc.x, startingGridLoc.y, false);
        pathTiles.Reverse();
        Debug.Log("Enemy path tile placement is finished.");
    }

    public bool pathLengthOkForDifficulty(int pathLength) {
        if (pathLength > Mathf.Abs(enemyPathStartingPos - enemyPathEndingPos) + gridHeight*2.2)
            if (Game.gameDifficulty == Difficulty.Easy)
                return true;
        if (pathLength > Mathf.Abs(enemyPathStartingPos - enemyPathEndingPos) + gridHeight*1.8 && (pathLength < Mathf.Abs(enemyPathStartingPos - enemyPathEndingPos) + gridHeight*2.2))
            if (Game.gameDifficulty == Difficulty.Normal)
                return true;
        if (pathLength > Mathf.Abs(enemyPathStartingPos - enemyPathEndingPos) + gridHeight*1.3 && (pathLength < Mathf.Abs(enemyPathStartingPos - enemyPathEndingPos) + gridHeight*1.8))
            if (Game.gameDifficulty == Difficulty.Hard)
                return true;
        return false;
    }

    private IEnumerator<float> _placeUserPlayableTiles(List<GameObject> tiles) {
        for (int i = 0; i < virtualGrid.Count; i++) {
            for (int j = 0; j < virtualGrid[i].Count; j++) {
                if (virtualGrid[i][j] == null) {
                    int tileIdx = Random.Range(0, tiles.Count);
                    bool placeTree = Random.Range(0f, 1f) < (float) Game.gameDifficulty/50f;
                    bool placeDestroyableObject = Random.Range(0f, 1f) < destroyableObjectGenerationProbabilty;
                    if (placeTree && placeDestroyableObject) {
                        CreateNewTile(tiles[tileIdx], i, j, isUserPlayable: null);
                        CreateNewTile(trees[Random.Range(0, trees.Count)], i, j, isUserPlayable: null);
                        Instantiate(
                            destroyableObjects[Random.Range(0, destroyableObjects.Count)],
                            new Vector3(gridToRealWorld[i][j].x + Random.Range(0, (int)tileScale/2), gridToRealWorld[i][j].y, gridToRealWorld[i][j].z + Random.Range(0, (int)tileScale/2)),
                            Quaternion.identity, neutralTiles.transform
                        );
                    } else if (placeTree) {
                        CreateNewTile(tiles[tileIdx], i, j, isUserPlayable: null);
                        CreateNewTile(trees[Random.Range(0, trees.Count)], i, j, isUserPlayable: null);
                    } else if (placeDestroyableObject) {
                        CreateNewTile(tiles[tileIdx], i, j, isUserPlayable: true);
                        Instantiate(
                            destroyableObjects[Random.Range(0, destroyableObjects.Count)],
                            new Vector3(gridToRealWorld[i][j].x + Random.Range(0, (int)tileScale/2), gridToRealWorld[i][j].y, gridToRealWorld[i][j].z + Random.Range(0, (int)tileScale/2)),
                            Quaternion.identity, neutralTiles.transform
                        );
                    } else {
                        CreateNewTile(tiles[tileIdx], i, j, isUserPlayable: true);
                    }
                    yield return Timing.WaitForOneFrame;
                }
            }
            // yield return Timing.WaitForOneFrame;
        }
    }

    private IEnumerator<float> _setupInitialTowers(CoroutineHandle waitForGameBoardSetup) {
        yield return Timing.WaitUntilDone(waitForGameBoardSetup);

        int towerCount;
        if (Game.gameDifficulty == Difficulty.Easy || Game.gameDifficulty == Difficulty.Normal) {
            towerCount = easyStartingTowerCount;
        } else {
            towerCount = hardStartingTowerCount;
        }

        Vector3 pos;
        int coveredSegmentSize = 0;
        int segmentSizes = playableTiles.Count/towerCount;
        int playableTileIdx;
        for (int i = 0; i < towerCount; i++) {
            playableTileIdx = Random.Range (coveredSegmentSize, Mathf.Clamp(coveredSegmentSize + segmentSizes, coveredSegmentSize, playableTiles.Count-1));
            pos = playableTiles[playableTileIdx].transform.position;
            if (Random.Range(0, 1f) > 1-tower4InitializationProbability)
                TowerController.towerController.BuildTower(TowerController.towerController.GetTowerBlueprintyByType(1, TowerType.NormalHitter), playableTilesVirtualPos[playableTileIdx]);
            else
                TowerController.towerController.BuildTower(TowerController.towerController.GetTowerBlueprintyByType(0, TowerType.NormalHitter), playableTilesVirtualPos[playableTileIdx]);
            yield return Timing.WaitForOneFrame;
            coveredSegmentSize += segmentSizes;
        }
        Debug.Log("Initial tower placement is complete.");
        gameBoardSetupComplete = true;
    }
    private IEnumerator<float> _gameBoardSetupCompleteStartWaveSpawner() {
        while (gameBoardSetupComplete != true) {
            yield return Timing.WaitForOneFrame;
        }
        WaveSpawner.waveSpawner.gameBoardSetupComplete();
    }
}