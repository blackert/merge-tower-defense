﻿using System.Collections;
using System.Collections.Generic;
using GameMain;
using UnityEngine;

using MEC;

public class BaseEnemy : MonoBehaviour
{
    public bool isBoss = false;
    private Transform spawnPoint;
    private Transform me;
    private Vector3 mePosition;

    protected float health = 100;
    protected float armor = 0;
    protected float speed = 5f;
    protected int coin = 5;

    private int pathIdx = 0;
    private Vector3 waypoint;
    public bool dead = false;
    private bool running = false;
    private bool pathEnded = false;

    private Animator animator;
    private string animatorMovementString;
    public bool runs = true;
    public bool walks = false;
    private bool enemyDying;
    private int enemyInWave;

    protected static int switchedEnemyAtRound; // Move to EnemyController (or whatever class to controll enemy caches.)

    void Start() {
        animator = GetComponent<Animator>();
        switchedEnemyAtRound = 0;
        // Init();
        Inactive();
        if (runs) {
            animatorMovementString = "run";
        } else if (walks) {
            animatorMovementString = "walk";
        } else {
            animatorMovementString = "run";
        }
    }

    protected virtual void Init() {
        enemyDying = false;
        me = transform;
        pathEnded = false;
        pathIdx = 0;
        waypoint = GameBoard.gameBoard.getNextPath(pathIdx);
    }

    public void SetEnemyInWave(int enemyInWave)
    {
        this.enemyInWave = enemyInWave;
    }

    public int GetEnemyInWave()
    {
        return enemyInWave;
    }
    
    public static void SwitchEnemyType() {
        switchedEnemyAtRound = WaveSpawner.waveCount;
    }

    public void setSpawnPoint(Transform _spawnPoint) {
        spawnPoint = _spawnPoint;
    }

    public float getHealth() {
        return health;
    }
    public void setHealth(float _health) {
        health = _health;
    }

    public float getArmor() {
        return armor;
    }
    public void setArmor(float _armor) {
        armor = _armor;
    }

    void Update() {
        mePosition = me.position;
        if (dead) return;
        if (Vector3.Distance(mePosition, waypoint) <= 0.5f) {
            waypoint = GameBoard.gameBoard.getNextPath(pathIdx++);
            if (waypoint != Vector3.zero)
                me.rotation = Quaternion.LookRotation(waypoint - mePosition, Vector3.up);
        }
        if (waypoint == Vector3.zero) {
            // path ended
            if (pathEnded) {
                Player.player.decreaseHealth();
                Inactive();
                return;
            } else {
                pathEnded = true;
                waypoint = mePosition + new Vector3(0, 0, GameBoard.gameBoard.tileScale);
            }
        }
        else {
            Move();
            // Vector3 dir = waypoint - mePosition;
            // me.Translate(dir.normalized * speed * Game.deltaTime, Space.World);
            mePosition = Vector3.MoveTowards(mePosition, waypoint, speed * Game.deltaTime);
            transform.position = mePosition;
        }
    }

    public virtual void Resurrect() {
        // Init();Init();
        me.position = spawnPoint.position;
        me.rotation = spawnPoint.rotation;
        gameObject.SetActive(true);
        dead = false;
    }

    public virtual void Inactive() {
        dead = true;
        gameObject.SetActive(false);
    }

    public bool IsDead() {
        return dead || enemyDying;
    }

    private void Move() {
        animator.SetBool(animatorMovementString, true);
    }

    private IEnumerator<float> _die() {
        animator.SetBool(animatorMovementString, false);
        animator.SetBool("die", true);
        Player.player.enemyKilled(coin, isBoss);
        WaveSpawner.waveSpawner.EnemyDead(enemyInWave);
        yield return Timing.WaitForSeconds(1f);
        Inactive();
    }

    protected void hit(float damage) {
        if (damage <= 0)
            return;
        if (health <= 0) {
            health = 0;
            return;
        }
        health = health - (damage - armor);
        if (health <= 0 && enemyDying == false) {
            health = 0;
            enemyDying = true;
            Timing.RunCoroutine(_die());
        }
    }
}
