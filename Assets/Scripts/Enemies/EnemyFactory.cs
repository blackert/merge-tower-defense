using System.Collections.Generic;
using UnityEngine;

using MEC;

public class EnemyFactory : MonoBehaviour
{
    public static EnemyFactory enemyFactory;

    public Transform EnemyParent;
    public Transform spawnPoint;

    public List<GameObject> enemyTypes;
    public int maxEnemyInstancesInCache = 1000;
    public int maxBossInstancesInCache = 100;
    public int burstCacheFilling = 5;

    public List<List<GameObject>> EnemyCachePerType;
    public List<List<GameObject>> BossCachePerType;

    public int globalEnemyTypeIndex;
    public int lastAccessedEnemyIndexInCache;
    private bool isBossRound;
    private int waveNumber;

    void Start() {
        enemyFactory = this;
    }

    public void InitRound() {
        globalEnemyTypeIndex = 0;
        CreateCacheSpaceForEnemies();
        CoroutineHandle enemyCacheCreateCoroutine = MEC.Timing.RunCoroutine(_populateEnemyCache());
        CoroutineHandle bossCacheCreateCoroutine = MEC.Timing.RunCoroutine(_populateBossCache());
        isBossRound = false;
    }

    public void EndRound() {
        Timing.RunCoroutine(_ClearCache(force: true));
    }

    public void InitWave(bool bossWave, int waveNumber)
    {
        isBossRound = bossWave;
        this.waveNumber = waveNumber;
    }

    public void EndWave()
    {
        isBossRound = false;
    }

    public GameObject GetEnemy()
    {
        GameObject fetchedEnemy;
        if (isBossRound)
        {
            fetchedEnemy = BossCachePerType[globalEnemyTypeIndex][lastAccessedEnemyIndexInCache];
        }
        else
        {
            fetchedEnemy = EnemyCachePerType[globalEnemyTypeIndex][lastAccessedEnemyIndexInCache];
        }

        lastAccessedEnemyIndexInCache = (lastAccessedEnemyIndexInCache + 1) % EnemyCachePerType[globalEnemyTypeIndex].Count;
        var enemy = fetchedEnemy.GetComponent<BasicEnemy>();
        enemy.Resurrect();
        enemy.SetEnemyInWave(waveNumber);
        return fetchedEnemy;
    }

    public void SwitchEnemyType() {
        BaseEnemy.SwitchEnemyType();
        ClearCacheEnemyType(globalEnemyTypeIndex);
        globalEnemyTypeIndex = (globalEnemyTypeIndex + 1) % enemyTypes.Count;
        lastAccessedEnemyIndexInCache = 0;
        CoroutineHandle enemyCacheCreateCoroutine = MEC.Timing.RunCoroutine(_populateEnemyCache());
        CoroutineHandle bossCacheCreateCoroutine = MEC.Timing.RunCoroutine(_populateBossCache());
    }

    private void CreateCacheSpaceForEnemies() {
        int enemyTypeCount = enemyTypes.Count;
        EnemyCachePerType = new List<List<GameObject>>();
        BossCachePerType = new List<List<GameObject>>();
        for (int i = 0; i < enemyTypeCount; i++) {
            EnemyCachePerType.Add(new List<GameObject>());
            BossCachePerType.Add(new List<GameObject>());
        }
    }

    private IEnumerator<float> _ClearCache(bool force=false) {
        for (int i = 0; i < EnemyCachePerType.Count; i++) {
            Timing.RunCoroutine(ClearCacheEnemyType(i, force));
            yield return Timing.WaitForOneFrame;
        }
    }

    private IEnumerator<float> ClearCacheEnemyType(int enemyTypeIdx, bool force=false) {
        int k = 0;
        foreach (GameObject enemyInstance in EnemyCachePerType[enemyTypeIdx]) {
            if (enemyInstance && (enemyInstance.activeInHierarchy == false || force)) {
                k++;
                Destroy(enemyInstance);
            }
            if (k % 50 == 0) {
                yield return Timing.WaitForOneFrame;
            }
        }
        foreach (GameObject enemyInstance in BossCachePerType[enemyTypeIdx]) {
            if (enemyInstance && (enemyInstance.activeInHierarchy == false || force)) {
                k++;
                Destroy(enemyInstance);
            }
            if (k % 50 == 0) {
                yield return Timing.WaitForOneFrame;
            }
        }
    }

    private GameObject spawnEnemy(GameObject enemy) {
        GameObject spawnedEnemy = (GameObject) Instantiate(enemy, EnemyParent.transform);
        spawnedEnemy.GetComponent<BaseEnemy>().setSpawnPoint(spawnPoint);
        return spawnedEnemy;
    }

    private IEnumerator<float> _populateEnemyCache() {
        GameObject enemy = enemyTypes[globalEnemyTypeIndex];
        int enemyRemainingInBurst = burstCacheFilling;
        List<GameObject> cache = EnemyCachePerType[globalEnemyTypeIndex];
        for(int i = 0; i < maxEnemyInstancesInCache; i++) {
            cache.Add(spawnEnemy(enemy));
            if (enemyRemainingInBurst <= 0) {
                enemyRemainingInBurst = burstCacheFilling;
                yield return Timing.WaitForOneFrame;
            }
            enemyRemainingInBurst--;
        }
        lastAccessedEnemyIndexInCache = 0;
    }
    
    private IEnumerator<float> _populateBossCache() {
        GameObject enemy = enemyTypes[globalEnemyTypeIndex];
        BasicEnemy enemyScript = enemy.GetComponent<BasicEnemy>();
        if (!enemyScript.isBoss)
        {
            enemy = enemyScript.GetBoss();
        }
        int enemyRemainingInBurst = burstCacheFilling;
        List<GameObject> cache = BossCachePerType[globalEnemyTypeIndex];
        for(int i = 0; i < maxEnemyInstancesInCache; i++) {
            cache.Add(spawnEnemy(enemy));
            if (enemyRemainingInBurst <= 0) {
                enemyRemainingInBurst = burstCacheFilling;
                yield return Timing.WaitForOneFrame;
            }
            enemyRemainingInBurst--;
        }
        lastAccessedEnemyIndexInCache = 0;
    }
}