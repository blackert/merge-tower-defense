﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class BasicEnemy : BaseEnemy
{
    [SerializeField] private GameObject bossEnemy;
    [SerializeField] private float enemyHealth = 100;
    [SerializeField] private float enemyArmor = 0;
    [SerializeField] private float enemySpeed = 3f;
    [SerializeField] private int enemyCoin = 5;
    private Slider healthSlider;

    [SerializeField] private float armorIncreasePerRound = 0.2f;
    [SerializeField] private float speedIncreasePerRound = 0.1f;
    [SerializeField] private float healthIncreasePerRound = 1;
    [SerializeField] private int rewardIncreasePerRound = 1;

    [SerializeField] private float armorCap = Mathf.Infinity;
    [SerializeField] private float speedCap = Mathf.Infinity;
    [SerializeField] private float healthCap = Mathf.Infinity;
    [SerializeField] private int rewardCap = 1000;
    
    [SerializeField] private int increaseDoublesInRound = 1;

    public int sinceLastUpdate = 0;

    private void Awake()
    {
        // Tell the Assert class to throw something on failure.
        UnityEngine.Assertions.Assert.raiseExceptions = true;
        if (!isBoss)
        {
            UnityEngine.Assertions.Assert.IsNotNull(bossEnemy, "Member \"bossEnemy\" is required.");
        } else if (isBoss)
        {
            UnityEngine.Assertions.Assert.IsNull(bossEnemy, "Member \"bossEnemy\" should be empty if the enemy is Boss.");
        }
    }

    protected override void Init() {
        base.Init();
        upgradeEnemy();
        if (sinceLastUpdate <= 0) {
            sinceLastUpdate = Mathf.Max(increaseDoublesInRound, 1);
            upgradeEnemy();
        }
        health = enemyHealth;
        armor = enemyArmor;
        speed = enemySpeed;
        coin = enemyCoin + WaveSpawner.waveCount;
        healthSlider = gameObject.GetComponentInChildren<Slider>();
    }

    public GameObject GetBoss()
    {
        return bossEnemy;
    }
    
    private void upgradeEnemy() {
        enemyArmor = Mathf.Clamp(enemyArmor + armorIncreasePerRound * (WaveSpawner.waveCount - switchedEnemyAtRound), 0, armorCap);
        enemySpeed = Mathf.Clamp(enemySpeed + speedIncreasePerRound * (WaveSpawner.waveCount - switchedEnemyAtRound) * 0.1f, 0, speedCap);
        enemyHealth = Mathf.Clamp(enemyHealth + healthIncreasePerRound * (WaveSpawner.waveCount - switchedEnemyAtRound), 0, healthCap);
        enemyCoin = Mathf.Clamp(enemyCoin + rewardIncreasePerRound * (WaveSpawner.waveCount - switchedEnemyAtRound), 0, rewardCap);
    }

    public override void Resurrect() {
        Init();
        base.Resurrect();
        // Debug.Log(("Resurrecting a new enemy with health: " + health + ", armor: " + armor + ", speed: " + speed + ", reward: " + coin));
    }

    public new void hit(float damage) {
        base.hit(damage);
        healthSlider.value = health / enemyHealth;
    }
}
