﻿using UnityEngine;
using UnityEngine.UI;

public class PurchasedItemFromIngameStore : MonoBehaviour
{
    public GameObject frame;
    public GameObject shopButton, cancelButton;
    public Toggle shopToggle;

    private IngameShopItem ingameShop;

    // Start is called before the first frame update
    void Start()
    {
        ingameShop = GetComponent<IngameShopItem>();
    }

    public void OnClick() {
        if (ingameShop.Bought()) {
            cancelButton.SetActive(true);
            shopButton.SetActive(false);
            frame.SetActive(false);
            shopToggle.isOn = false;
        }
        else {
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject (null);
        }
    }
}
