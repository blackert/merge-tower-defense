﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabGroup : MonoBehaviour
{
    [HideInInspector] public List<Tab> tabs = new List<Tab>();
    public PanelGroup panelGroup;

    public Tab activeTab;

    public List<GameObject> objectsToSwap;

    private void Start() {
        StartActiveTab();
    }

    public void StartActiveTab() {
        if (activeTab != null) {
            SetActive(activeTab);
        }
    }

    public void ClearActiveTab() {
        activeTab = null;
    }

    public void SetActive(int siblingIndex) {
        foreach (Tab tab in tabs) {
            if (tab.transform.GetSiblingIndex() == siblingIndex) {
                SetActive(tab);
            }
        }
    }

    public void SetActive(Tab tab) {
        OnTabSelected(tab);
    }

    public void Subscribe (Tab tab) {
        tabs.Add(tab);
    }

    public void OnTabEnter(Tab tab) {
        ResetTabs();
        if (activeTab == null || tab != activeTab) {
            tab.setColor(tab.highlightColors);
        }
    }

    public void OnTabExit(Tab tab) {
        ResetTabs();
    }

    public void OnTabSelected(Tab tab) {
        if (activeTab != null) {
            activeTab.Deselect();
        }
        activeTab = tab;
        activeTab.Select();
        ResetTabs();
        tab.setColor(tab.selectedColors);
        int index = tab.transform.GetSiblingIndex();
        for (int i = 0; i < objectsToSwap.Count; i++) {
            if (i == index) {
                objectsToSwap[i].SetActive(true);
            }
            else {
                objectsToSwap[i].SetActive(false);
            }
        }

        if (panelGroup != null) {
            panelGroup.SetPageIndex(tab.transform.GetSiblingIndex());
        }
    }

    public void ResetTabs() {
        foreach(Tab button in tabs) {
            if (button != activeTab)
                button.ResetTab();
        }
    }

    public void DisableTab(int index) {
        if (tabs.Count > index)
            tabs[index].Disable();
    }

    public void EnableTab(int index) {
        if (tabs.Count > index)
            tabs[index].Enable();
    }
}
