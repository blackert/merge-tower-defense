﻿using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;

public class MainMenuUIController : MonoBehaviour
{
    public BackgroundCameraWander cameraWander;
    public UITweener mainMenuTweener;
    public GameOverPanelScript gameOverPanel;
    public GameObject MainMenuPanel, PlayPanel;

    public IEnumerator<float> Enable() {
        gameObject.SetActive(true);
        transform.GetComponent<CanvasGroup>().enabled = false;
        yield return Timing.WaitForOneFrame;
        cameraWander.OnEnable();
        yield return Timing.WaitForOneFrame;
        mainMenuTweener.OnEnable();
    }

    public CoroutineHandle GameOver() {
        Timing.RunCoroutine(Enable());
        return gameOverPanel.OnEnable();
    }

    public void Disable() {
        cameraWander.OnDisable();
        mainMenuTweener.OnDisable();
        gameOverPanel.OnDisable();
        MainMenuPanel.SetActive(false);
        PlayPanel.SetActive(false);
    }
}
