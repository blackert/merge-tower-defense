﻿using TMPro;
using UnityEngine;

public class BetRaidPredictionsController : MonoBehaviour
{
    public static BetRaidPredictionsController betRaidPredictionsController;

    public TextMeshProUGUI predictedWaveNumberText;

    public int raidLevelPrediction = 0;
    public int raidBetAmount = 0;
    public int potentialGains = 0;

    private void Awake()
    {
        betRaidPredictionsController = this;
        Clear();
    }

    public void InitRound()
    {
        
    }
    
    public int EndRound(int lastFinishedWave)
    {
        int earntMoney;
        if (lastFinishedWave >= raidLevelPrediction)
        {
            Debug.Log("Player managed to finish betted level " + raidLevelPrediction);
            Debug.Log("Will earn reward of " + potentialGains);
            earntMoney = potentialGains;
        }
        else
        {
            Debug.Log("Player failed to finish betted level" + raidLevelPrediction);
            earntMoney = 0;
        }
        Clear();
        return earntMoney;
    }

    private void Clear()
    {
        raidLevelPrediction = 0;
        raidBetAmount = 0;
        potentialGains = 0;
    }

    public int GetRaidLevelPrediction()
    {
        return raidLevelPrediction;
    }

    public int GetRaidBetAmount()
    {
        return raidBetAmount;
    }
}
