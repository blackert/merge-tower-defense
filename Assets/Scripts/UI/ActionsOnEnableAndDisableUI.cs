using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class ActionsOnEnableAndDisableUI : MonoBehaviour
{
    public List<GameObject> objectsToDisableWhenEnabled;
    public List<GameObject> objectsToEnableWhenDisabled;

    void OnEnable() {
        foreach (GameObject t in objectsToDisableWhenEnabled) {
            t.SetActive(false);
        }
    }

    void OnDisable() {
        foreach (GameObject t in objectsToEnableWhenDisabled) {
            t.SetActive(true);
        }
    }
}
