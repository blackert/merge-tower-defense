﻿using GameMain;
using UnityEngine;

public class UIEnabledPauseGame : MonoBehaviour
{
    void OnEnable() {
        Game.game.UIActivate();
        Game.Pause();
    }

    void OnDisable() {
        Game.game.UIDeactivate();
        Game.Unpause();
    }
}
