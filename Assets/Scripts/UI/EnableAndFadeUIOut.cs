﻿using System;
using System.Collections.Generic;
using MEC;
using UnityEngine;

public class EnableAndFadeUIOut : MonoBehaviour
{
    public UITweener uiTweener;
    public bool shouldFadeOut;

    private void Awake()
    {
        shouldFadeOut = true;
    }

    void OnEnable() {
        if (shouldFadeOut)
        {
            Timing.RunCoroutine(_fadeOut());
        }
    }

    IEnumerator<float> _fadeOut() {
        yield return Timing.WaitForSeconds(2f*Time.timeScale);
        uiTweener.OnDisable();
        // gameObject.SetActive(false);
    }
}
