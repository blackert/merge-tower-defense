﻿using UnityEngine;
using UnityEngine.UI;

using TMPro;


[ExecuteInEditMode]
public class GetTabName : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<TextMeshProUGUI>().text = transform.parent.gameObject.name;
    }
}
