﻿using UnityEngine;
using TMPro;

public class ShowGameSpeedUI : MonoBehaviour
{
    public TextMeshProUGUI gameSpeedText;

    public void OnClick() {
        Debug.Log("Time scale text changed.");
        gameSpeedText.text = Time.timeScale.ToString() + "x";
    }
}
