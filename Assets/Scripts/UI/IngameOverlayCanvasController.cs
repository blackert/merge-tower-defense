﻿using System.Collections.Generic;
using MEC;
using UnityEngine;
public class IngameOverlayCanvasController : MonoBehaviour
{
    public GameObject ingameMoney, ingameWaveInfo, ingameShopButton, ingameShopFrame;
    public GameObject gameOverSad, gameOverHappy, gameOverWaitForTouch;

    public void InitRound() {
        ingameMoney.SetActive(true);
        ingameWaveInfo.SetActive(true);
        ingameShopButton.SetActive(true);
        gameObject.SetActive(true);
    }

    public IEnumerator<float> EndRound() {
        EnableGameOverIngameOverlay();
        ingameWaveInfo.SetActive(false);
        ingameShopButton.SetActive(false);
        ingameShopFrame.SetActive(false);
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(_Delay()));
    }

    private void EnableGameOverIngameOverlay() {
        gameOverSad.SetActive(true);
        gameOverHappy.SetActive(true);
        gameOverWaitForTouch.SetActive(true);
    }
    private void DisableGameOverIngameOverlay() {
        gameOverSad.SetActive(false);
        gameOverHappy.SetActive(false);
        gameOverWaitForTouch.SetActive(false);
    }

    private IEnumerator<float> _Delay() {
        yield return Timing.WaitForSeconds(1f);
        Debug.Log("touch to continue");
        var pressed = false;
        while(!pressed){
            if (Input.GetMouseButtonUp(0))
            {
                pressed = true;
            }
            yield return Timing.WaitForOneFrame;
        }
        DisableGameOverIngameOverlay();
    }
}
