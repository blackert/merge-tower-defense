using System.Collections;
using UnityEngine;

public class PanelGroup : MonoBehaviour {
    public GameObject[] panels;

    // public TabGroup tabGroup;

    public int panelIndex;

    private void Awake() {
        ShowCurrentPanel();
    }

    void OnEnable() {
        ShowCurrentPanel();
    }

    void ShowCurrentPanel() {
        for (int i = 0; i < panels.Length; i++) {
            GameObject panel = panels[i].gameObject;
            if (i == panelIndex) {
                panel.SetActive(true);
                foreach(UITweener tileTweener in panel.GetComponentsInChildren<UITweener>()) {
                    tileTweener.Show();
                }
            }
            panel.SetActive(i==panelIndex);
        }
    }

    public void SetPageIndex(int index) {
        panelIndex = index;
        ShowCurrentPanel();
    }

}