﻿using System.Collections.Generic;
using MEC;
using TMPro;
using UnityEngine;

public class GameOverPanelScript : MonoBehaviour
{
    public GameObject WaveInfoCanvas;

    public TextMeshProUGUI enemiesKilledText;
    public TextMeshProUGUI bossesKilledText;
    public TextMeshProUGUI netWorthText;
    public TextMeshProUGUI raidLevelPredictionText;
    public TextMeshProUGUI earnedInRaidText;
    private bool gameOverPanelOn;

    public CoroutineHandle OnEnable()
    {
        gameOverPanelOn = true;
        WaveInfoCanvas.GetComponent<EnableAndFadeUIOut>().shouldFadeOut = false;
        WaveInfoCanvas.SetActive(true);
        enemiesKilledText.text = "Enemies killed: " + Player.player.GetNumberOfEnemiesKilled();
        bossesKilledText.text = "Bosses killed: " + Player.player.GetNumberOfBossesKilled();
        netWorthText.text = "Net worth in game: " + Player.player.GetNetWorthInLastRound();
        raidLevelPredictionText.text = 
            "Raid level prediction (Achieved): " 
            + BetRaidPredictionsController.betRaidPredictionsController.GetRaidLevelPrediction() 
            + " (" + WaveSpawner.waveCount +")";
        earnedInRaidText.text = "Earned in raid: " + Player.player.GetMoneyEarnedLastRound();
        gameObject.SetActive(true);
        return Timing.RunCoroutine(_WaitUntilGameOverCanvasGone());
    }

    private IEnumerator<float> _WaitUntilGameOverCanvasGone()
    {
        yield return Timing.WaitForSeconds(2f);
        while (gameOverPanelOn)
        {
            yield return Timing.WaitForOneFrame;
        }
    }

    public void OnDisable()
    {
        WaveInfoCanvas.GetComponent<EnableAndFadeUIOut>().shouldFadeOut = true;
        WaveInfoCanvas.SetActive(false);
        gameOverPanelOn = false;
        gameObject.SetActive(false);
    }

}
