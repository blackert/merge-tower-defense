﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.EventSystems;
using UnityEngine.Events;


public class Tab : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public TabGroup tabGroup;

    public List<Graphic> graphics;
    public List<Color> normalColors;
    public List<Color> selectedColors;
    public List<Color> highlightColors;
    public List<Color> disabledColors;

    public UnityEvent onTabSelected;
    public UnityEvent onTabDeselected;

    private UITweener tweener;
    private IngameShopItem ingameShop;

    private bool enabled;

    public void OnPointerClick(PointerEventData eventData) {
        if (enabled) {
            tabGroup.OnTabSelected(this);
        }
    }

    public void OnPointerEnter(PointerEventData eventData) {
        if (enabled) {
            tabGroup.OnTabEnter(this);
        }
    }

    public void OnPointerExit(PointerEventData eventData) {
        if (enabled) {
            tabGroup.OnTabExit(this);
        }
    }

    void Start() {
        tweener = GetComponent<UITweener>();
        ingameShop = GetComponent<IngameShopItem>();
        setColor(normalColors);
        tabGroup.Subscribe(this);
        enabled = true;
    }

    public void setColor(List<Color> graphicColors) {
        for (int i = 0; i < Mathf.Min(graphicColors.Count, graphics.Count); i++) {
            graphics[i].color = graphicColors[i];
        }
    }

    public void ResetTab() {
        setColor(normalColors);
    }

    public void Select() {
        if (tweener != null) {
            tweener.Show();
        }
        if (ingameShop != null) {
            ingameShop.Bought();
        }
        if (onTabSelected != null) {
            onTabSelected.Invoke();
        }
    }

    public void Deselect() {
        if (onTabDeselected != null) {
            onTabDeselected.Invoke();
        }
        Enable();
    }

    public void Enable() {
        enabled = true;
        setColor(normalColors);
    }

    public void Disable() {
        enabled = false;
        setColor(disabledColors);
    }
}
