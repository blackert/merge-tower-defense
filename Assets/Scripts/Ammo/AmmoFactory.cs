using System.Collections.Generic;
using UnityEngine;

using MEC;

public class AmmoFactory : MonoBehaviour
{
    public static AmmoFactory ammoFactory;

    public Transform AmmoParent;

    public List<GameObject> ammoTypes;
    public int maxAmmoInstancesInCache = 5000;
    public int burstCacheFilling = 5;

    public List<List<GameObject>> ammoCache;

    public List<int> lastAccessedAmmoIndexInCache;

    void Start() {
        ammoFactory = this;
        CreateCacheSpaceForAmmos();
        CoroutineHandle ammoCacheCreateCoroutine = Timing.RunCoroutine(_populateCache());
        // ClearCache();
    }

    public void EndRound()
    {
        Timing.RunCoroutine(DisableAllAmmo());
    }

    public GameObject FireBullet(AmmoType ammoType, Transform _target, Transform _firePoint, float _bulletDamage, float _bulletSpeed, float _bulletExplosionRadius, EffectType _effectType) {
        int ammoTypeIdx = (int)ammoType;
        GameObject fetchedAmmo = ammoCache[ammoTypeIdx][lastAccessedAmmoIndexInCache[ammoTypeIdx]];
        lastAccessedAmmoIndexInCache[ammoTypeIdx] = (lastAccessedAmmoIndexInCache[ammoTypeIdx] + 1) % ammoCache[ammoTypeIdx].Count;

        Bullet bullet = fetchedAmmo.GetComponent<Bullet>();
        if (bullet != null) {
            bullet.Resurrect(_target, _firePoint, _bulletDamage, _bulletSpeed, _bulletExplosionRadius, _effectType);
        }
        return fetchedAmmo;
    }

    private void CreateCacheSpaceForAmmos() {
        int ammoTypeCount = ammoTypes.Count;
        ammoCache = new List<List<GameObject>>();
        lastAccessedAmmoIndexInCache = new List<int>();
        for (int i = 0; i < ammoTypeCount; i++) {
            ammoCache.Add(new List<GameObject>());
            lastAccessedAmmoIndexInCache.Add(0);
        }
    }

    private IEnumerator<float> DisableAllAmmo()
    {
        for (int i = 0; i < ammoCache.Count; i++)
        {
            int k = 0;
            foreach (GameObject ammoInstance in ammoCache[i])
            {
                if (ammoInstance.activeInHierarchy)
                {
                    k++;
                    ammoInstance.GetComponent<Bullet>().Inactive();
                }
                if (k % 10 == 0)
                {
                    yield return Timing.WaitForOneFrame;
                }
            }
        }
    }

    private void ClearCache() {
        for (int i = 0; i < ammoCache.Count; i++) {
            ClearCacheAmmoType(i);
        }
    }

    private void ClearCacheAmmoType(int ammoTypeIdx) {
        Debug.Log("Before clear on row " + ammoTypeIdx + " size:" + ammoCache[ammoTypeIdx].Count);
        foreach (GameObject ammoInstance in ammoCache[ammoTypeIdx]) {
            Destroy(ammoInstance);
        }
        Debug.Log("After clear on row " + ammoTypeIdx + " size:" + ammoCache[ammoTypeIdx].Count);
    }

    private GameObject spawnAmmo(GameObject ammo) {
        GameObject spawnedAmmo = (GameObject) Instantiate(ammo, AmmoParent.transform);
        return spawnedAmmo;
    }

    private IEnumerator<float> _populateCache() {
        int ammoRemainingInBurst = burstCacheFilling;
        for(int i = 0; i < ammoCache.Count; i++) {
            GameObject ammo = ammoTypes[i];
            List<GameObject> cache = ammoCache[i];
            lastAccessedAmmoIndexInCache[i] = 0;
            for(int j = 0; j < maxAmmoInstancesInCache; j++) {
                cache.Add(spawnAmmo(ammo));
                if (ammoRemainingInBurst <= 0) {
                    ammoRemainingInBurst = burstCacheFilling;
                    yield return Timing.WaitForOneFrame;
                }
                ammoRemainingInBurst--;
            }
        }
    }
}