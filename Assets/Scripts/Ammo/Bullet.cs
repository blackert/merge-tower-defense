using GameMain;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public EffectType impactEffect;
    protected Transform target;
    private Vector3 targetPos;
    protected float damage;
    protected float speed;
    protected float explosionRadius;

    public virtual void Init(Transform _target, float _damage, float _speed, float _explosionRadius, EffectType effectType) {
        target = _target;
        targetPos = target.position;
        damage = _damage;
        speed = _speed;
        explosionRadius = _explosionRadius;
        impactEffect = effectType;
    }

    void Start() {
        impactEffect = EffectType.Bullet;
        Inactive();
    }

    void Update() {
        // if (target == null || !target.gameObject.activeInHierarchy) {
        //     HitTarget();
        //     return;
        // }
        Vector3 dir = targetPos - transform.position;
        float distanceThisFrame = speed * Game.deltaTime;

        if (dir.magnitude <= distanceThisFrame) {
            HitTarget();
            return;
        }
        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(targetPos);
    }

    private void HitTarget() {
        EffectsFactory.effectsFactory.ShowEffect(impactEffect, transform);

        if (explosionRadius >= 1f) {
            Explode();
        } else {
            Damage(target, damage);
        }

        Inactive();
    }

    public virtual void Resurrect(Transform _target, Transform _firePoint, float _damage, float _speed, float _explosionRadius, EffectType effectType) {
        transform.position = _firePoint.position;
        transform.rotation = _firePoint.rotation;
        Init(_target, _damage, _speed, _explosionRadius, effectType);
        gameObject.SetActive(true);
    }

    public virtual void Inactive() {
        gameObject.SetActive(false);
        target = null;
    }

    private void Explode() {
        Collider[] hitObjects = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider collider in hitObjects)
        {
            if (collider.tag == "Enemies") {
                float dist = Vector2.Distance(transform.position, collider.transform.position);
                if (dist > explosionRadius) {
                    continue;
                }
                float explosionDamage = damage * (explosionRadius - dist)/explosionRadius;
                if (explosionDamage < 0) {
                    Debug.Log("SOMETHING IS WRONG, explosionRadius:" + explosionRadius + ", enemy pos: " + collider.transform.position + ", dist: " + dist);
                }
                if (dist <= 1) explosionDamage = damage;
                else explosionDamage = damage / dist;
                Damage(collider.transform, explosionDamage);
            }
        }
    }
    private void Damage(Transform enemy, float damage) {
        if (enemy == null) return;
        BasicEnemy _enemy = enemy.gameObject.GetComponent<BasicEnemy>();
        if (_enemy != null) {
            _enemy.hit(damage);
        }
    }

}
