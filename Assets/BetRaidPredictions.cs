﻿using System;
using GameMain;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BetRaidPredictions : MonoBehaviour
{
    public TextMeshProUGUI betRaidLevelPredictionText;
    public TextMeshProUGUI betRaidAmountText;
    public TextMeshProUGUI betPotentialGainsText;

    public void OnRaidLevelPredictionValueChanged(Slider slider)
    {
        BetRaidPredictionsController.betRaidPredictionsController.raidLevelPrediction = (int) (slider.value * Game.game.maxBetRaidLevel / 10f) * 10;
        betRaidLevelPredictionText.text = "Will survive raid #: " + BetRaidPredictionsController.betRaidPredictionsController.raidLevelPrediction;
        BetRaidPredictionsController.betRaidPredictionsController.predictedWaveNumberText.text = BetRaidPredictionsController.betRaidPredictionsController.raidLevelPrediction.ToString("000");
        CalculatePotentialGains();
    }

    public void OnRaidAmountBetValueChanged(Slider slider)
    {
        BetRaidPredictionsController.betRaidPredictionsController.raidBetAmount = (int) (slider.value * Player.player.GetStashMoney());
        betRaidAmountText.text = "Will risk from stash: " + BetRaidPredictionsController.betRaidPredictionsController.raidBetAmount;
        CalculatePotentialGains();
    }

    private void CalculatePotentialGains()
    {
        float difficultyMultiplier = Game.game.GetDifficultyMultiplier();
        var raidLevelMultiplier = (float)BetRaidPredictionsController.betRaidPredictionsController.raidLevelPrediction / 10 * difficultyMultiplier;
        BetRaidPredictionsController.betRaidPredictionsController.potentialGains = (int) (raidLevelMultiplier * BetRaidPredictionsController.betRaidPredictionsController.raidBetAmount) + BetRaidPredictionsController.betRaidPredictionsController.raidBetAmount;
        betPotentialGainsText.text = "Potential Gains: " + BetRaidPredictionsController.betRaidPredictionsController.potentialGains;
    }
}
